<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>

<html>
    <body>
        <div style="background: #d6d6d5; display: flex; font-family: 'HelveticaNeueBold','HelveticaNeue-Bold','Helvetica Neue Bold',Helvetica,Arial,sans-serif; font-size: 14px;">
            <div style="margin: 0 auto;">
                <div style="width: 700px; background: #d6d6d5; display: flex;">
                    <div style="margin: 0 auto;">
                        <div style="width: 500px; background: #fff; padding: 30px; margin-top: 50px;">
                            <div style="width: 100%; text-align: left;">
                                <a href="http://luxmeter.cl/iot/lampmonitor ?>" target="_blank">
                                    <img src="http://luxmeter.cl/iot/lampmonitor/img/lux_logo.png" height="60" style="margin-top:-5px;"/>
                                </a>
                                <span style="font-weight:300; color:#444; font-size: 28px; margin-top:10px; margin-left:10px; display: ruby-text;"><?= Yii::$app->name ?></span>
                            </div>
                            <?php if ( isset($pre_content) && $pre_content != '' ) { ?>
                                <div style="width: 100%; text-align: center; font-size: 28px !important; line-height: 40px !important; padding-bottom: 14px;">
                                    <?= $pre_content ?>
                                </div>
                            <?php } ?>
                            <div style="width: 100%; text-align: left; font-size: 14px !important; line-height: 22px !important; padding-bottom: 32px;">
                                <?= $content ?>
                            </div>
                            <div style="width: 100%; text-align: left; font-size: 14px !important; line-height: 20px !important; padding-bottom: 7px; padding-top: 7px;">
                                <a href="http://luxmeter.cl/iot/lampmonitor" target="_blank" style="background-color: #101010; border-color: #101010; border-radius: 0px; border-style: solid; border-width: 13px 16px; color: #ffffff; display: inline-block; letter-spacing: 1px; max-width: 300px; min-width: 100px; text-align: center; text-decoration: none;">
                                    <?= Yii::t('app', 'Go to LampMonitor') ?> <span style="padding-left: 10px; float: right; padding-top: 2px; display: inline-block;"> <img width="13" height="12" src="http://lafabrica.generovalor.com/~gic/img/arrow.png"></span>
                                </a>
                            </div>
                            
                        </div>
                        <div style="background: #b00000; padding: 30px; text-align: center; color: #fff; font-size: 12px; margin-bottom: 50px;">
                            © Luxmeter <?= date('Y') ?>.
                        </div>
                    </div>
                </div>
		    </div>
		</div>
    </body>
</html>