<?php

use yii\db\Migration;

/**
 * Class m190903_140114_tabla_bitacora_conexion
 */
class m190903_140114_tabla_bitacora_conexion extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('bitacora_conexion', [
            'id' => $this->primaryKey(),
            'ultima_conexion' => $this->dateTime(),
            'id_centro' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-bitacora_conexion-id_centro','bitacora_conexion','id_centro');
        $this->addForeignKey('fk-bitacora_conexion-id_centro', 'bitacora_conexion', 'id_centro', 'centro', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190903_140114_tabla_bitacora_conexion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190903_140114_tabla_bitacora_conexion cannot be reverted.\n";

        return false;
    }
    */
}
