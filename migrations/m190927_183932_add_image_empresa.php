<?php

use yii\db\Migration;

/**
 * Class m190927_183932_add_image_empresa
 */
class m190927_183932_add_image_empresa extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn ( 'empresa', 'url_imagen', $this->string(255) );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190927_183932_add_image_empresa cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190927_183932_add_image_empresa cannot be reverted.\n";

        return false;
    }
    */
}
