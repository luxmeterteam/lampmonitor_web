<?php

use yii\db\Migration;

/**
 * Class m190830_145936_tablas_asoc_usuarios
 */
class m190830_145936_tablas_asoc_usuarios extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        /* Empresas asociadas a un usuario */
        $this->createTable('user_empresa', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer(),
            'id_empresa' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-user_empresa-id_user','user_empresa','id_user');
        $this->addForeignKey('fk-user_empresa-id_user', 'user_empresa', 'id_user', 'user', 'id', 'CASCADE');
        
        $this->createIndex('idx-user_empresa-id_empresa','user_empresa','id_empresa');
        $this->addForeignKey('fk-user_empresa-id_empresa', 'user_empresa', 'id_empresa', 'empresa', 'id', 'CASCADE');
        
        /* Centros asociados a un usuario */
        $this->createTable('user_centro', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer(),
            'id_centro' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-user_centro-id_user','user_centro','id_user');
        $this->addForeignKey('fk-user_centro-id_user', 'user_centro', 'id_user', 'user', 'id', 'CASCADE');
        
        $this->createIndex('idx-user_centro-id_centro','user_centro','id_centro');
        $this->addForeignKey('fk-user_centro-id_centro', 'user_centro', 'id_centro', 'centro', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190830_145936_tablas_asoc_usuarios cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190830_145936_tablas_asoc_usuarios cannot be reverted.\n";

        return false;
    }
    */
}
