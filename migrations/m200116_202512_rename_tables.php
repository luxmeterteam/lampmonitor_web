<?php

use yii\db\Migration;

/**
 * Class m200116_202512_rename_tables
 */
class m200116_202512_rename_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameTable("estado_led", "lm_estado_led");
        $this->renameTable("lectura_led", "lm_lectura_led");
        $this->renameTable("centro_email_notification", "lm_centro_email_notification");
        $this->renameTable("user_notification", "lm_user_notification");
        $this->renameTable("event_centro", "lm_event_centro");
        $this->renameTable("bitacora_conexion", "lm_bitacora_conexion");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200116_202512_rename_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200116_202512_rename_tables cannot be reverted.\n";

        return false;
    }
    */
}
