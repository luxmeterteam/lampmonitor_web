<?php

use yii\db\Migration;

/**
 * Class m190905_121535_add_imagen_centro
 */
class m190905_121535_add_imagen_centro extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn ( 'centro', 'url_imagen', $this->string(255) );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190905_121535_add_imagen_centro cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190905_121535_add_imagen_centro cannot be reverted.\n";

        return false;
    }
    */
}
