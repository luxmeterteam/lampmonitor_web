<?php

use yii\db\Migration;

/**
 * Class m190923_205254_create_notifications
 */
class m190923_205254_create_notifications extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('user_notification', [
            'id' => $this->primaryKey(),
            'date' => $this->dateTime(),
            'type' => $this->integer(),
            'cant_lamps' => $this->integer(),
            'lamps' => $this->string(255),
            'id_user' => $this->integer(),
            'id_centro' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-user_notification-id_user','user_notification','id_user');
        $this->addForeignKey('fk-user_notification-id_user', 'user_notification', 'id_user', 'user', 'id', 'CASCADE');

        $this->createIndex('idx-user_notification-id_centro','user_notification','id_centro');
        $this->addForeignKey('fk-user_notification-id_centro', 'user_notification', 'id_centro', 'centro', 'id', 'CASCADE');

        $this->addCommentOnColumn ('user_notification', 'type', '0-Apagado, 1-Encendido, 2-Falla Lámpara');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190923_205254_create_notifications cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190923_205254_create_notifications cannot be reverted.\n";

        return false;
    }
    */
}
