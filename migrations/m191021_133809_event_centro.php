<?php

use yii\db\Migration;

/**
 * Class m191021_133809_event_centro
 */
class m191021_133809_event_centro extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('event_centro', [
            'id' => $this->primaryKey(),
            'cant_lamps_on' => $this->integer(),
            'cant_lamps_off' => $this->integer(),
            'cant_lamps_reconnected' => $this->integer(),
            'cant_lamps_not' => $this->integer(),
            'cant_lamps' => $this->integer(),
            'mail_send' => $this->boolean(),
            'date' => $this->dateTime(),
            'index' => $this->integer(),
            'id_centro' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-event_centro-id_centro','event_centro','id_centro');
        $this->addForeignKey('fk-event_centro-id_centro', 'event_centro', 'id_centro', 'centro', 'id', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191021_133809_event_centro cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191021_133809_event_centro cannot be reverted.\n";

        return false;
    }
    */
}
