<?php

use yii\db\Migration;

/**
 * Class m200124_132932_add_lectura_led
 */
class m200124_132932_add_lectura_led extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn( 'lm_lectura_led', 'id_modulo', $this->integer() );
        $this->addColumn( 'lm_lectura_led', 'modulo', $this->string(150) );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200124_132932_add_lectura_led cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200124_132932_add_lectura_led cannot be reverted.\n";

        return false;
    }
    */
}
