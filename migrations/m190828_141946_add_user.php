<?php

use yii\db\Migration;

/**
 * Class m190828_141946_add_user
 */
class m190828_141946_add_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn ( 'user', 'nombre', $this->string(25) );
        $this->addColumn ( 'user', 'apellido', $this->string(25) );
        $this->alterColumn( 'user', 'created_at', $this->dateTime() );
        $this->alterColumn( 'user', 'updated_at', $this->dateTime() );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190828_141946_add_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190828_141946_add_user cannot be reverted.\n";

        return false;
    }
    */
}
