<?php

use yii\db\Migration;

/**
 * Class m191002_125429_add_user
 */
class m191002_125429_add_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn ( 'user', 'url_imagen', $this->string(255) );
        $this->addColumn ( 'user', 'direccion', $this->string(255) );
        $this->addColumn ( 'user', 'telefono', $this->string(40) );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191002_125429_add_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191002_125429_add_user cannot be reverted.\n";

        return false;
    }
    */
}
