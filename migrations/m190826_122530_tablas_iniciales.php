<?php

use yii\db\Migration;

/**
 * Class m190826_122530_tablas_iniciales
 */
class m190826_122530_tablas_iniciales extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('estado_led', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45),
            'color' => $this->string(10),
        ], $tableOptions);

        $this->createTable('empresa', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45),
            'codigo_empresa' => $this->string(45),
            'fecha_creacion' => $this->dateTime(),
            'fecha_actualizacion' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable('centro', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45),
            'codigo_centro' => $this->integer(),
            'direccion' => $this->string(150),
            'fecha_creacion' => $this->dateTime(),
            'fecha_actualizacion' => $this->dateTime(),
            'id_empresa' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-empresa_centro','centro','id_empresa');
        $this->addForeignKey('fk-empresa_centro', 'centro', 'id_empresa', 'empresa', 'id', 'CASCADE');

        $this->createTable('lectura_led', [
            'id' => $this->primaryKey(),
            'codigo_centro' => $this->integer(),
            'jaula' => $this->string(20),
            'lampara' => $this->integer(),
            'nserie' => $this->string(30),
            'hora_lectura' => $this->time(),
            'fecha_lectura' => $this->date(),
            'fecha_actualizacion_remota' => $this->dateTime(),
            'id_centro' => $this->integer()->notNull(),
            'id_estado' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-centro_lectura_led','lectura_led','id_centro');
        $this->addForeignKey('fk-centro_lectura_led', 'lectura_led', 'id_centro', 'centro', 'id', 'CASCADE');
        
        $this->createIndex('idx-estado_lectura_led','lectura_led','id_estado');
        $this->addForeignKey('fk-estado_lectura_led', 'lectura_led', 'id_estado', 'estado_led', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190826_122530_tablas_iniciales cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190826_122530_tablas_iniciales cannot be reverted.\n";

        return false;
    }
    */
}
