<?php

use yii\db\Migration;

/**
 * Class m191002_183607_centro_email_notification
 */
class m191002_183607_centro_email_notification extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('centro_email_notification', [
            'id' => $this->primaryKey(),
            'cargo' => $this->string(45),
            'email' => $this->string(255)->notNull(),
            'id_user' => $this->integer()->null(),
            'id_centro' => $this->integer(),
            'date' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('idx-centro_email_notification-id_user','centro_email_notification','id_user');
        $this->addForeignKey('fk-centro_email_notification-id_user', 'centro_email_notification', 'id_user', 'user', 'id');

        $this->createIndex('idx-centro_email_notification-id_centro','centro_email_notification','id_centro');
        $this->addForeignKey('fk-centro_email_notification-id_centro', 'centro_email_notification', 'id_centro', 'centro', 'id', 'CASCADE');

        $this->dropForeignKey('fk-user_notification-id_user', 'user_notification');
        $this->dropIndex('idx-user_notification-id_user', 'user_notification');

        $this->dropColumn('user_notification', 'id_user');

        $this->addColumn('user_notification', 'id_centro_email', $this->integer());

        $this->createIndex('idx-user_notification-id_centro_email','user_notification','id_centro_email');
        $this->addForeignKey('fk-user_notification-id_centro_email', 'user_notification', 'id_centro_email', 'centro_email_notification', 'id', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191002_183607_centro_email_notification cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191002_183607_centro_email_notification cannot be reverted.\n";

        return false;
    }
    */
}
