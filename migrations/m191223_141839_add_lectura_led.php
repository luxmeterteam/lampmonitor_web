<?php

use yii\db\Migration;

/**
 * Class m191223_141839_add_lectura_led
 */
class m191223_141839_add_lectura_led extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn( 'lectura_led', 'potencia', $this->float() );
        $this->addColumn( 'lectura_led', 'voltaje', $this->float() );
        $this->addColumn( 'lectura_led', 'corriente', $this->float() );
        $this->addColumn( 'lectura_led', 'id_lampara', $this->integer() );
        $this->addColumn( 'lectura_led', 'id_local', $this->integer() );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191223_141839_add_lectura_led cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191223_141839_add_lectura_led cannot be reverted.\n";

        return false;
    }
    */
}
