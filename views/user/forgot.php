<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'Forgot password?');

?>
<div class="forgot">
    <br>
    <br>
    <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            <div class="text-left">
                <?php $form = ActiveForm::begin([ 'enableAjaxValidation' => true ]); ?>
                    <h3><?= Yii::t('app', 'Forgot your password?')?></h3>
                    <p><?= Yii::t('app', 'Enter your email address to reset your password. You may need to check your spam folder or unblock noreplyluxmeter@gmail.com.') ?></p>
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => Yii::t('app', 'Email') ])->label(false) ?>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group text-center">
                                <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-red']) ?>
                            </div>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
                
            </div>
        </div>
    </div>

</div>