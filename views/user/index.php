<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';

?>
<div class="user-index">
    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h2><img src="<?= Yii::$app->request->baseUrl ?>/img/user.png" alt=""></h2>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 10px;">
            <h2><?= Html::encode(Yii::t('app', $this->title)) ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="pull-right">
                <?= Html::a(Yii::t('app', 'Register User'),['create'], ['class' => 'btn btn-red']); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'options' =>[
                    'class' => 'grid table-striped responsive',
                ],
                'formatter' => [
                    'class' => 'yii\i18n\Formatter',
                    'nullDisplay' => '-',
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'nombre',
                    'apellido',
                    'username',
                    'email',
                    'telefono',
                    'direccion',
                    // 'auth_key',
                    // 'password_hash',
                    // 'password_reset_token',
                    //'status',
                    //'created_at',
                    //'updated_at',
                    [
                        'attribute' => 'rol',
                        'label' => Yii::t('app', 'Role'),
                        'format' => 'raw',
                        'value' => function($model){
                            $roles = Yii::$app->authManager->getRolesByUser($model->id);
                            $string_role = '';
                            foreach ($roles as $key => $rol) {
                                $string_role .= '<span class="label label-status" style="background-color: #ec1b30;">'.ucfirst($rol->name).'</span>';
                            }
                            return $string_role;
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> $filter_roles,
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>Yii::t('app', 'Select')],  
                    ],

                    [
                        'class' => '\kartik\grid\ActionColumn',
                        'header'=> Yii::t('app', 'Actions'),
                        'template' => '{view} {update}',
                        'buttons' => [
                            //view button
                            'view' => function ($url, $model){
                                return Html::a('<span><i class="fa fa-eye"></i></span>',[ 'view', 'id' => $model->id ], [ 'title' => Yii::t('app', 'View User') ]);
                            },
                            //Edit button
                            'update' => function ($url, $model){
                                if (Yii::$app->user->can('admin')) {
                                    return Html::a('<span><i class="fa fa-pencil"></i></span>',[ 'update', 'id' => $model->id ], [ 'title' => Yii::t('app', 'Update User') ]);
                                }
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
    

</div>
