<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'User Profile');

?>
<div class="user-profile">
    <div class="row">
        <div class="col-md-10 col-xs-9">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="pull-right">
                <?= Html::a(Yii::t('app', 'Back'),['site/index'], ['class' => 'btn btn-red']); ?>
            </div>
        </div>
    </div>
    <br>
    <br>
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'apellido')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="col-md-12">
                <label class="control-label" for="user-telefono"><?= Yii::t('app', 'Phone Number') ?></label>
            </div>
            <div class="col-md-3 prefix">
                <?=
                    $form->field($model, 'prefix')->widget(Select2::classname(), [
                        'data' => $prefixes,
                        //'value' => ($model->prefix)?$model->prefix:'',
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'allowClear' => false,
                        ],
                    ])->label(false);
                ?>
            </div>
            <div class="col-md-9">
                <?= $form->field($model, 'telefono')->textInput(['maxlength' => true])->label(false) ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?=
                $form->field($model, "image")->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' => [
                        'showPreview' => true,
                        'initialPreviewAsData'=>true,
                        'initialPreview' => [
                            $model->url_imagen ? Yii::$app->request->baseUrl.$model->url_imagen : Yii::$app->request->baseUrl."/img/user_gray.jpg", // checks the models to display the preview
                        ],
                        'overwriteInitial' => true,
                    ],
                    
                ]);
            ?>
        </div>
    </div>

    <br>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-red']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    div.prefix{
        padding-right: 0px;
    }
</style>
<script>
    $('#user-prefix').on('change', function(event) {
        console.log($('#user-prefix').val());
    });
</script>