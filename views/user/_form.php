<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'apellido')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?=  
                $form->field($model, 'rol')->widget(Select2::classname(), [
                    'data' => $model_item,
                    'options' => ['placeholder' => Yii::t('app', 'Select'),],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
            ?>
        </div>
        <div id="si-admin-empresa" style= <?= (!$model->isNewRecord && $model->rol === 'admin-empresa')?'': '"display: none;"'?>>

            <?php if (Yii::$app->user->can('admin') ) { ?>
                <div class="col-md-4 col-xs-12">
                    <?=
                        $form->field($model_user_empresa, 'id_empresa')->widget(Select2::classname(), [
                            'data' => $empresas,
                            'options' => ['placeholder' => Yii::t('app', 'Companies'),],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(Yii::t('app', 'Associated Company'));
                    ?>
                </div>
            <?php } ?>

        </div>
        <div id="si-admin-centro" style= <?= (!$model->isNewRecord && $model->rol === 'admin-centro')?'': '"display: none;"'?>>

            <?php if (Yii::$app->user->can('admin') ) { ?>
                <div class="col-md-4 col-xs-12">
                    <?=
                        $form->field($model_user_centro, 'id_empresa')->widget(Select2::classname(), [
                            'data' => $empresas,
                            'options' => ['placeholder' => Yii::t('app', 'Select'), 'id' => 'id_empresa'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ])->label(Yii::t('app', 'Company'));
                    ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4 col-xs-12">
                    <?=
                        $form->field($model_user_centro, 'id_centro')->widget(DepDrop::classname(), [
                            'type' => DepDrop::TYPE_SELECT2,
                            'data' => (($model_user_centro->id_centro)?[$model_user_centro->id_centro=>'']:[''=>'']),
                            'options'=>['id'=>'id_centro', 'placeholder' => Yii::t('app', 'Select'),],
                            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                            'pluginOptions'=>[
                                'depends'=>['id_empresa'],
                                'url'=>Url::to(['/user/getcentros']),
                                'initialize' => true,
                            ]
                        ])->label(Yii::t('app', 'Associated Farm'));
                    ?>
                </div>
            <?php } ?>

        </div>

        <?php if (!$model->isNewRecord) { ?>
            <div class="col-md-4 col-xs-12">
                <?= $form->field($model, 'newPassword')->passwordInput() ?>
            </div>
            <div class="col-md-4 col-xs-12">
                <?= $form->field($model, 'retypePassword')->passwordInput() ?>
            </div>
        <?php } ?>
    </div>
    <br>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-red']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $('#user-rol').on('change', function(event) {
        event.preventDefault();
        /* Act on the event */
        console.log($(this).val());
        if ( $(this).val() == 'admin-empresa' ) {
            $("#si-admin-empresa").show();
            $("#si-admin-centro").hide();
        } else if ( $(this).val() == 'admin-centro'){
            $("#si-admin-centro").show();
            $("#si-admin-empresa").hide();
        } else{
            $("#si-admin-empresa").hide();
            $("#si-admin-centro").hide();
        }
    });
</script>
