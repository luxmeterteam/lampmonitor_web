<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'Create User');

?>
<div class="user-create">
    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h2><img src="<?= Yii::$app->request->baseUrl ?>/img/user_add.png" alt=""></h2>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 10px;">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="pull-right">
                <?= Html::a(Yii::t('app', 'Back'),['index'], ['class' => 'btn btn-red']); ?>
            </div>
        </div>
    </div>
    <br>
    <br>

    <?= $this->render('_form', [
        'model' => $model,
        'model_item' => $model_item,
        'model_user_empresa' => $model_user_empresa,
        'empresas' => $empresas,
        'model_user_centro' => $model_user_centro,
        'centros' => $centros,
    ]) ?>

</div>
