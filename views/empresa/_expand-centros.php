<?php

use app\models\BitacoraConexion;
use yii\helpers\Html;
use kartik\grid\GridView;

?>
<br>
<div class="row text-left">

    <div class="col-xs-10 col-xs-offset-1">
        <p><b><?= Yii::t('app', 'Fish Farms') ?></b></p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider_centros,
            'responsiveWrap' => false,
            'summary' => '',
            'tableOptions' => ['class' => 'responsive', 'id' => 'centros-grid'],
            'formatter' => [
                'class' => 'yii\i18n\Formatter',
                'nullDisplay' => '',
            ],
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'headerOptions' => ['style' => 'width:50px;  white-space: normal;', 'class' => 'hidde-mobile'],
                    'contentOptions' => ['class' => 'hidde-mobile'],
                    'filterOptions' => ['class' => 'hidde-mobile'],
                ],
                [
                    'attribute' => 'codigo_centro',
                    'format' => 'text',
                    'headerOptions' => ['style' => 'width:200px;  white-space: normal;', 'class' => 'hidde-mobile'],
                    'contentOptions' => ['class' => 'hidde-mobile'],
                    'filterOptions' => ['class' => 'hidde-mobile'],
                ],
                [
                    'attribute' => 'nombre',
                    'format' => 'text',
                    'value' => function($model){
                        return ucfirst($model->nombre);
                    },
                ],
                [
                    'attribute' => 'direccion',
                    'headerOptions' => ['class' => 'hidde-mobile'],
                    'contentOptions' => ['class' => 'hidde-mobile'],
                    'filterOptions' => ['class' => 'hidde-mobile'],
                ],
                [
                    'attribute' => 'fecha_creacion',
                    'format' => ['date', 'php:d-m-Y'],
                    'visible' => $config,
                    'headerOptions' => ['class' => 'hidde-mobile'],
                    'contentOptions' => ['class' => 'hidde-mobile'],
                    'filterOptions' => ['class' => 'hidde-mobile'],
                ],

                [
                    'label' => Yii::t('app', 'Last connection'),
                    //'format' => ['date', 'php:d-m-Y H:i'],
                    'visible' => !$config,
                    'value' => function($model){
                        $model_bitacora = BitacoraConexion::find()->where(['id_centro' => $model->id])->one();
                        if (isset($model_bitacora->ultima_conexion)) {
                            return $model_bitacora->ultima_conexion;
                        }
                        return '-';
                    },
                    'headerOptions' => ['class' => 'hidde-mobile'],
                    'contentOptions' => ['class' => 'hidde-mobile'],
                    'filterOptions' => ['class' => 'hidde-mobile'],
                ],

                [
                    'class' => '\kartik\grid\ActionColumn',
                    'header'=> Yii::t('app', 'Actions'),
                    'template' => '{conf} {view}',
                    'buttons' => [
                        //conf button
                        'conf' => function ($url, $model) use ($config){
                            if ($config) {
                                return Html::a('<span><i class="fa fa-cog"></i></span>',[ 'configuracion', 'id' => $model->id ], [ 'title' => 'Configurar Centro', 'class' => 'btn btn-default' ]);
                            }
                            
                        },
                        //view button
                        'view' => function ($url, $model) use ($config){
                            if (!$config) {
                                return Html::a('<span><i class="fa fa-eye"></i> '.Yii::t('app', 'View details').'</span>',[ '/centro/detallecentro', 'id' => $model->id ], [ 'title' => Yii::t('app', 'Center'), 'class' => 'btn btn-default']);
                            }
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>
    
</div>

<style>
    @media screen and (max-width: 400px) {
        .hidde-mobile{
            display:none;
        }
    }
</style>

