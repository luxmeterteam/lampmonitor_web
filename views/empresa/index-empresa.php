<?php

use app\models\CentroSearch;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmpresaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Companies');
?>
<div class="empresa-index">
    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h2><img src="<?= Yii::$app->request->baseUrl ?>/img/factory.png" alt=""></h2>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 10px;">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsiveWrap' => false,
                'options' =>[
                    'class' => 'grid table-striped responsive',
                ],
                'formatter' => [
                    'class' => 'yii\i18n\Formatter',
                    'nullDisplay' => '',
                ],
                'columns' => [
                    [
                        'class' => 'kartik\grid\ExpandRowColumn',
                        'width' => '50px',
                        'expandIcon' => '<i class="fa fa-caret-right" aria-hidden="true" style="color:#343f44;"></i>',
                        'collapseIcon' => '<i class="fa fa-caret-down" aria-hidden="true" style="color:#343f44;"></i>',
                        'value' => function ($model, $key, $index, $column) {
                            return GridView::ROW_COLLAPSED;
                        },
                        'detail' => function ($model, $key, $index, $column) {
                            //Items
                            $searchModel_centros = new CentroSearch(['id_empresa' => $model->id]);
                            $dataProvider_centros = $searchModel_centros->search(Yii::$app->request->queryParams);

                            return Yii::$app->controller->renderPartial('_expand-centros.php', ['model' => $model, 'dataProvider_centros' => $dataProvider_centros, 'config' => false]);
                        },
                        'headerOptions' => ['class' => 'kartik-sheet-style'],
                        'expandOneOnly' => true,
                    ],
                    ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['style' => 'width:50px;  white-space: normal;', 'class' => 'hidde-mobile'], 'contentOptions' => ['class' => 'hidde-mobile'], 'filterOptions' => ['class' => 'hidde-mobile'],],
                    [
                        'attribute' => 'codigo_empresa',
                        'headerOptions' => ['style' => 'width:200px;  white-space: normal;', 'class' => 'hidde-mobile'],
                        'contentOptions' => ['class' => 'hidde-mobile'],
                        'filterOptions' => ['class' => 'hidde-mobile'],
                    ],
                    [
                        'attribute' => 'nombre',
                        'headerOptions' => ['style' => 'width:300px;  white-space: normal;'],
                    ],
                    [
                        'class' => '\kartik\grid\ActionColumn',
                        'header'=> Yii::t('app', 'Actions'),
                        'template' => '{centros}',
                        'buttons' => [
                            //view button
                            'centros' => function ($url, $model){
                                return Html::a('<span><i class="fa fa-eye"></i> '.Yii::t('app', 'View details').'</span>',[ '/centro/indexempresa', 'id' => $model->id ], [ 'title' => Yii::t('app', 'Centers'), 'class' => 'btn btn-default']);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<style>
    @media screen and (max-width: 400px) {
        .hidde-mobile{
            display:none;
        }
    }
</style>
