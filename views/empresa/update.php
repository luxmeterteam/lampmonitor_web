<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Empresa */

$this->title = Yii::t('app', 'Update Company') . ': ' . ucfirst($model->nombre);
?>
<div class="empresa-update">

    <div class="row">
         <div class="col-md-1 col-xs-3">
            <h2><img src="<?= Yii::$app->request->baseUrl ?>/img/factory.png" alt=""></h2>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 20px;">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="pull-right">
                <?= Html::a(Yii::t('app', 'Back'),['index'], ['class' => 'btn btn-red']); ?>
            </div>
        </div>
    </div>
    <br>
    <br>

    <?= $this->render('_form', [
        'model' => $model,
        'model_centros' => $model_centros,
    ]) ?>

</div>
