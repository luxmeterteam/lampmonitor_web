<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Empresa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empresa-form">

    <?php $form = ActiveForm::begin([ 'id' => 'dynamic_lista-form', /*'enableAjaxValidation' => true*/ ]); ?>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'codigo_empresa')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?=
                $form->field($model, "image")->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' => [
                        'showPreview' => false,
                        'initialPreviewAsData'=>true,
                        'initialPreview' => [
                            $model->url_imagen ? Yii::$app->request->baseUrl.$model->url_imagen : null, // checks the models to display the preview
                        ],
                        'overwriteInitial' => true,
                    ],
                    
                ]);
            ?>
        </div>
    </div>
    <br>
    <br>
    <div class="row">
         <div class="col-md-1 col-xs-3">
            <h2><img src="<?= Yii::$app->request->baseUrl ?>/img/fish.png" alt=""></h2>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 20px;">
            <h2><?= Html::encode(Yii::t('app', 'Centers')) ?></h2>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper_resultados',
                'widgetBody' => '.container-centros',
                'widgetItem' => '.centro',
                'limit' => 10,
                'min' => 1,
                'insertButton' => '.add-centro',
                'deleteButton' => '.remove-centro',
                'model' => $model_centros[0],
                'formId' => 'dynamic_lista-form',
                'formFields' => [
                    'codigo_centro',
                    'nombre',
                    'direccion',
                    'image'
                ],
            ]); ?>
            <button type="button" class="add-centro btn btn-default pull-right"><?= Yii::t('app', 'Add Center') ?></button>
            <table class="table table-hover">
                <tbody class="container-centros">
                <?php foreach ($model_centros as $i => $model_centro): ?>
                    <tr class="centro panel panel-default">
                        <td class="vcenter">
                            <?php
                                // necessary for update action.
                                if (!$model_centro->isNewRecord) {
                                    echo Html::activeHiddenInput($model_centro, "[{$i}]id");
                                }
                            ?>
                            
                            <div class="col-xs-12 col-md-3"><?= $form->field($model_centro, "[{$i}]codigo_centro")->textInput(); ?></div>
                            <div class="col-xs-12 col-md-3"><?= $form->field($model_centro, "[{$i}]nombre")->textInput(); ?></div>
                            <div class="col-xs-12 col-md-4"><?= $form->field($model_centro, "[{$i}]direccion")->textInput(); ?></div>

                            <div class="col-xs-12 col-md-1 text-center pull-right" >
                                <button type="button" class="remove-centro btn btn-danger btn-xs" style="margin-top:28px;">
                                    <i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-12 col-md-10">
                                <?php $id = 'centro-'.$i.'-image'; ?>
                                <?=
                                    $form->field($model_centro, "[{$i}]image")->widget(FileInput::classname(), [
                                        'options' => ['accept' => 'image/*', 'id' => $id],
                                        'pluginOptions' => [
                                            'showPreview' => false,
                                            'showCaption' => true,
                                            'showRemove' => true,
                                            'showUpload' => true,
                                            'initialPreview' => [
                                                $model_centro->url_imagen ? Html::img(Yii::$app->request->baseUrl.$model_centro->url_imagen) : null, // checks the models to display the preview
                                            ],
                                            'overwriteInitial' => true,
                                        ],
                                    ]);
                                ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php DynamicFormWidget::end(); ?>
            
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-red']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<style>
    .table > tbody > tr > td, .table > tfoot > tr > td {
        border-top: none !important;
    }

    .panel {
        border: none !important;
    }
</style>

<script>
    $(".dynamicform_wrapper_resultados").on("afterInsert", function(e, item) {
        $(".dynamicform_wrapper_resultados .centro:last-child input").val('');
    });
</script>
