<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $model app\models\Centro */

$this->title = Yii::t('app', 'Cage Detail')/*. ': ' . $model_centro->nombre . " - " . $jaula_numero*/;

/* Estados */
$estados = [
    0 => "dot-no",
    1 => "dot-on",
    2 => "dot-off",
    3 => "dot-notconn"
];

$gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],
    'id',
    'fecha_lectura',
    'hora_lectura',
    [
        'attribute' => 'id_centro',
        'value' => function($model){
            return $model->centro->nombre;
        }
    ],
    'modulo',
    'jaula',
    'lampara',
    'nserie',
    [
        'attribute' => 'id_estado',
        'value' => function($model){
            return $model->estado->nombre;
        }
    ],
    [
        'attribute' => 'voltaje',
        'visible' => Yii::$app->user->can('admin')
    ],
    [
        'attribute' => 'corriente',
        'visible' => Yii::$app->user->can('admin')
    ],
    [
        'attribute' => 'potencia',
        'visible' => Yii::$app->user->can('admin')
    ],
];

?>
<div class="detalle-jaula">
    <div class="row">
        <div class="col-md-12">
            <h1><?= Html::encode($this->title) ?></h1>
            <h2><?= $model_centro->nombre. ' - '. "C" . $jaula_numero ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="pull-right">
                <?= Html::a(Yii::t('app', 'Fish Farm'),['detallecentro', 'id' => $id_centro], ['class' => 'btn btn-red']); ?>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <?php foreach ($series as $key => $serie) { ?>
                <div class="col-xs-3 col-md-2">
                    <span class="nro-lampara"> <?= Yii::t('app', 'Lamp') ." ". ($key+1) ?> </span>
                </div>
                <div class="col-xs-9 col-md-10">
                    <?=
                        ExportMenu::widget([
                            'dataProvider' => $dataProvider[$key],
                            'columns' => $gridColumns,
                            'target' => ExportMenu::TARGET_SELF,
                            'showConfirmAlert' => false,
                            'clearBuffers' => true,
                            'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_HTML => false,
                            ]
                        ]);
                    ?>
                </div>
                <hr>
                <div class="col-xs-12">
                    <?=
                        \onmotion\apexcharts\ApexchartsWidget::widget([
                            'type' => 'line', // default area
                            'height' => '200', // default 350
                            //'width' => '500', // default 100%
                            'chartOptions' => [
                                'chart' => [
                                    'toolbar' => [
                                        'show' => true,
                                        'autoSelected' => 'zoom'
                                    ],
                                ],
                                'tooltip' => [
                                    'enabled' => true,
                                    /*'shared' => true,
                                    'theme' => 'light',*/
                                    'x' => [
                                        'show' => true,
                                        'format' => 'dd MMM HH:mm', // dd/MM, dd MMM yy, dd MMM yyyy
                                    ],
                                    'y' => [
                                        'show' => false,
                                        'formatter' => "function(value) { return value + '$' }",
                                        /*'title' => [
                                            'formatter' => "function(val) {return val.toFixed(2) + '$';}",
                                        ],*/
                                    ],
                                    'marker' => [
                                        'show' => false,
                                    ],
                                    'items' => [
                                        'display' => 'flex',
                                    ],
                                ],
                                'xaxis' => [
                                    'type' => 'datetime',
                                    // 'categories' => $categories,
                                ],
                                'yaxis' => [
                                    'tickAmount' => 2,
                                    'min' => 0,
                                    'max' => 2,
                                    'forceNiceScale' => true,
                                    'labels' => [
                                        'formatter' => new \yii\web\JsExpression("function(value){ if(value == 2){ return 'ON'; } if(value == 1){ return 'OFF'; } else{ return ''; } }"),
                                    ],
                                ],
                                'plotOptions' => [
                                    'bar' => [
                                        'horizontal' => false,
                                        //'endingShape' => 'rounded'
                                        'columnWidth' => '70%',
                                        'barHeight' => '70%',
                                        'distributed' => false,
                                    ],
                                ],
                                'dataLabels' => [
                                    'enabled' => false,
                                    'formatter' => new \yii\web\JsExpression("function(value){ if(value == 2){ return 'ON'; } if(value == 1){ return 'OFF'; } else{ return ''; } }"),
                                ],
                                'stroke' => [
                                    'show' => true,
                                    'colors' => ['#2651fc'],
                                    'curve' => 'stepline',
                                    'lineCap' => 'square',
                                ],
                                'legend' => [
                                    'verticalAlign' => 'bottom',
                                    'horizontalAlign' => 'left',
                                ],
                                'markers' => [
                                    'colors' => ['#00628e'],
                                    'size' => 4
                                ],
                                /*'title' => [
                                    'text' => 'Lampara '.($key+1),
                                    'align' => 'left',
                                    'margin' => 10,
                                    'offsetX' => 0,
                                    'offsetY' => 0,
                                    'floating' => false,
                                    'style' => [
                                        'fontSize' =>  '16px',
                                        'color' =>  '#263238'
                                    ],
                                ],*/
                            ],
                            'series' => $serie
                        ]);
                    ?>
                    <hr>
                </div>
                <br>
            <?php } ?>
            <?php if (count($series) == 0) { ?>
                <div class="alert alert-success" role="alert">
                    <p><strong><?= Yii::t('app', 'Information') ?>:</strong> <?= Yii::t('app', 'No lamps readings in the last 7 days') ?></p>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<style>
    .nro-lampara{
        font-weight: 500;
        line-height: 1.1;
        color: inherit;
        font-size: 28px;
    }
</style>