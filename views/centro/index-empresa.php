<?php

//use yii\helpers\Html;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Empresa */

$this->title = Yii::t('app', 'Fish Farms');
?>
<div class="index-empresa">
    <div class="row">
        <div class="col-md-12">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
    <?php if (Yii::$app->user->can('admin') || Yii::$app->user->can('admin-luxmeter')) { ?>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="pull-right">
                    <?= Html::a(Yii::t('app', 'List of Companies'),['empresa/indexempresa'], ['class' => 'btn btn-red']); ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <br>
    <div class="row">

        <?php foreach ($model_centros as $key => $centro) { ?>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="thumbnail">
                    <?php 
                        $url_imagen = (!isset($centro->url_imagen))?"/img/map.png":$centro->url_imagen;
                    ?>
                    <img src="<?= Yii::$app->request->baseUrl.$url_imagen ?>" alt="Centro: <?= $centro->nombre ?>" style="width:300px; height:200px;">
                    <div class="caption">
                        <h3><?= $centro->nombre ?></h3>
                        <p>
                            <?= Yii::t('app', 'Address') .": ". $centro->direccion ?><br>
                            <?= Yii::t('app', 'Last connection') ?>:
                            <?php foreach ($model_bitacora_conexion as $key => $conexion) {
                                if ($conexion->id_centro == $centro->id) {
                                    echo date('d-m-Y H:i', strtotime($conexion->ultima_conexion));
                                }
                            } ?>
                        </p>
                        <br>
                        <p class="text-right">
                            <?php if (\Yii::$app->user->can('[Empresa] Notificaciones')) { ?>
                                <?= Html::a(Yii::t('app', 'Notifications'),['centro/notifications', 'id' => $centro->id], ['class' => 'btn btn-default']); ?>
                            <?php } ?>
                            <?= Html::a(Yii::t('app', 'View'),['detallecentro', 'id' => $centro->id], ['class' => 'btn btn-red']); ?>
                        </p>
                    </div>
                </div>
            </div>
        <?php } ?>
        
    </div>
    
</div>