<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Empresa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notification-form">

    <?php $form = ActiveForm::begin([ 'id' => 'dynamic_lista-form', /*'enableAjaxValidation' => true*/ ]); ?>

    <div class="row">
         <div class="col-md-1 col-xs-3">
            <h2><img src="<?= Yii::$app->request->baseUrl ?>/img/email.png" alt=""></h2>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 20px;">
            <h2><?= Html::encode(Yii::t('app', 'Notification Emails') . ' - ' . $model->nombre) ?></h2>
        </div>

    </div>
    <br>
    <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper_resultados',
                'widgetBody' => '.container-centros',
                'widgetItem' => '.centro',
                'limit' => 10,
                'min' => 1,
                'insertButton' => '.add-centro',
                'deleteButton' => '.remove-centro',
                'model' => $model_centro_email[0],
                'formId' => 'dynamic_lista-form',
                'formFields' => [
                    'cargo',
                    'email'
                ],
            ]); ?>
            <button type="button" class="add-centro btn btn-default pull-right"><?= Yii::t('app', 'Add Email') ?></button>
            <table class="table table-hover">
                <tbody class="container-centros">
                <?php foreach ($model_centro_email as $i => $model_centro): ?>
                    <tr class="centro panel panel-default">
                        <td class="vcenter">
                            <?php
                                if (! $model_centro->isNewRecord) {
                                    echo $form->field($model_centro, "[{$i}]id")->label(false)->hiddenInput([ 'placeholder'=>'id', 'value' => $model_centro->id]);
                                }
                            ?>
                            
                            <div class="col-xs-12 col-md-5"><?= $form->field($model_centro, "[{$i}]cargo")->textInput(); ?></div>
                            <div class="col-xs-12 col-md-5"><?= $form->field($model_centro, "[{$i}]email")->textInput(); ?></div>
                            <?= Html::activeHiddenInput($model_centro, "[{$i}]id_user", ["class" => "id-user"]); ?>

                            <div class="col-xs-12 col-md-2 text-center pull-right" >
                                <button type="button" class="remove-centro btn btn-danger btn-xs" style="margin-top:28px;">
                                    <i class="glyphicon glyphicon-minus"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php DynamicFormWidget::end(); ?>
            
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group text-center">
            <?= Html::a(Yii::t('app', 'Back'),['indexempresa', 'id' => $model->empresa->id], ['class' => 'btn btn-red']); ?>
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-red']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<style>
    .table > tbody > tr > td, .table > tfoot > tr > td {
        border-top: none !important;
    }

    .panel {
        border: none !important;
    }
</style>

<script>
    $(".dynamicform_wrapper_resultados").on("afterInsert", function(e, item) {
        $(".dynamicform_wrapper_resultados .centro:last-child input").val('');
    });

    $( document ).ready(function() {
        $(".centro input.id-user").each(function(index) {
            if ($(this).val() != "") {
                var id_cargo = '#centroemailnotification-'+index+'-cargo';
                var id_email = '#centroemailnotification-'+index+'-email';

                $(id_cargo).prop( "disabled", true );
                $(id_email).prop( "disabled", true );
                $(this).closest('td').find('.remove-centro').prop( "disabled", true );
            }
        });
    });
</script>
