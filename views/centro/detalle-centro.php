<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $model app\models\Empresa */

$this->title = Yii::t('app', 'Fish Farm Detail') . ': ' . $model_centro->nombre;

/* Estados */
$estados = [
    0 => "dot-no",
    1 => "dot-on",
    2 => "dot-off",
    3 => "dot-notconn"
];

$gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],
    'id',
    'fecha_lectura',
    'hora_lectura',
    [
        'attribute' => 'id_centro',
        'value' => function($model){
            return $model->centro->nombre;
        }
    ],
    'modulo',
    'jaula',
    'lampara',
    'nserie',
    [
        'attribute' => 'id_estado',
        'value' => function($model){
            return $model->estado->nombre;
        }
    ],
    [
        'attribute' => 'voltaje',
        'visible' => Yii::$app->user->can('admin')
    ],
    [
        'attribute' => 'corriente',
        'visible' => Yii::$app->user->can('admin')
    ],
    [
        'attribute' => 'potencia',
        'visible' => Yii::$app->user->can('admin')
    ],
];

?>
<div class="detalle-centro">
    <div class="row">
        <div class="col-md-12">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
    <?php if ( !Yii::$app->user->can('admin-centro') ) { ?>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="pull-right">
                    <?= Html::a(Yii::t('app', 'Fish Farms'),['indexempresa', 'id' => $model_centro->id_empresa], ['class' => 'btn btn-red']); ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <br>
    <br>
    <div class="row">
        <?php for ($i=0; $i < $modulos ; $i++) { ?>
            <div class="col-xs-12">
                <h4>
                    <?= Yii::t('app', 'Module') . ": " . (count($nombre_modulos) > 0?$nombre_modulos[$i]:'000') ?> 
                    <span class="pull-right">
                        <?=
                            ExportMenu::widget([
                                'dataProvider' => $dataProvider[$i],
                                'columns' => $gridColumns,
                                'target' => ExportMenu::TARGET_SELF,
                                'showConfirmAlert' => false,
                                'clearBuffers' => true,
                                'exportConfig' => [
                                    ExportMenu::FORMAT_TEXT => false,
                                    ExportMenu::FORMAT_HTML => false,
                                ],
                            ]);
                        ?>
                    </span>
                </h4>
            </div>
            <div class="clearfix"></div>
            <br>
            <?php if ($tipo_jaula == 1) { ?>
                <div class="col-xs-12">
                    <?php 
                        for ($j=0; $j < $jaulas; $j++) {
                            $cod_jaula = (isset($nombre_modulos[$i][0])?$nombre_modulos[$i][0]:($j+1)).(($j+1 < 10)?"0":"").($j+1);
                    ?>
                        <a id="jaula-<?= $j ?>" class="jaula-tipo1" 
                            <?php
                                $content = "";
                                if ($estatus_jaulas[$i+1][$j] != 0) {
                                    $content = 'href="'. Url::toRoute(['centro/detallejaula', 'id' => $cod_jaula, 'id_centro' => $model_centro->id ]). '" rel="popover" data-content="<ul class='."'list-unstyled'".'>';
                                    foreach ($estatus_lamparas[$i][$j] as $key => $lamp) {
                                        $content .= '<li>'. Yii::t('app', 'Lamp'). " " .$key.':  <span class='."'dot-popover " . $estados[$lamp] . " pull-right'".' ></span></li>';
                                    }
                                    $content .= '</ul>"';
                                    echo $content;
                                } 
                            ?>>
                            <div class="col-xs-12">C<?= $cod_jaula ?></div>
                            <div class="col-xs-12 middle">
                                <span class="dot <?= $estados[$estatus_jaulas[$i+1][$j]]?>"></span>
                            </div>
                        </a>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="clearfix"></div>
            <br>
        <?php } ?>
        <?php if (isset($model_bitacora_conexion)) { ?>
            <div class="col-xs-12">
                <br>
                <p class="text-right">
                    <?= Yii::t('app', 'Last connection') ?>: <?= date('d-m-Y H:i', strtotime($model_bitacora_conexion->ultima_conexion)) ?>
                </p>
            </div>
        <?php } ?>
        <br><br><br>
        <div class="col-xs-12">
            <div class="response-chart"></div>
        </div>
    </div>
    
</div>

<style>
    .jaula-tipo1{
        height: 120px; 
        width: calc(100% / <?= $jaulas ?>); 
        /*background-color: red; */
        float: left;
        border: 1px solid;
        border-color: #c9ced4;
        box-shadow: 0 10px 10px rgba(20,23,28,.25);
    }

    @media(max-width:767px) {
        .jaula-tipo1{
            height: 150px; 
            width: 100%;
        }
        .dot {
            height: 120px !important;
            width: 120px !important;
            display: inline-block !important;
        }
        .middle {
            margin: 0 auto !important;
        }
    }

    .middle {
        text-align:center;
        position:relative;
        display: inline-block;
        border-spacing: 1px;
        margin-top: 5px;
    }

    .dot {
        height: 80px;
        width: 80px;
        background-color: #bbb;
        border-radius: 50%;
        display: table-cell;
    }

    .dot-popover {
        margin-top: 2px;
        height: 15px;
        width: 15px;
        border-radius: 50%;
        display: table-cell;
    }

    .dot-on{
        background-color: #36ff72;
    }
    .dot-off{
        background-color: #f55;
    }
    .dot-notconn{
        background-color: #ffeb47;
    }
    .dot-no{
        background-color: #ccc;
    }

    a:hover, a:active, a:focus {
        outline: none;
        text-decoration: none;
        color: #515151;
    }
    a {
        color: #000;
    }
</style>

<script>
  //Cuando la página esté cargada completamente se recargara cada 5min
    $(document).ready(function(){
        setTimeout("location.reload(true);", 300000);

        $('a[rel=popover]').popover({
            html: true,
            trigger: 'hover',
            placement: 'bottom',
        });

        
    });
</script>