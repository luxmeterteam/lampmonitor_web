<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/site.css');
$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="row login-contenedor">
    
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 img-contenedor">
        <img src="<?= Yii::$app->request->baseUrl ?>/img/banner-second.jpg" class="fondo-img" width="100%"/>
    </div>

    <div class="col-xs-12 col-sm-12  col-md-4  col-lg-4 form-contenedor">
        <div class="login-box-body">
            <div class="row">
                <div class="col-xs-12">
                    <?= Alert::widget() ?>
                </div>
            </div>
            <div class="login-logo">
                <a href="#">
                    <span class="logo-lg"><img src="<?= Yii::$app->request->baseUrl ?>/img/lux_logo.png" height="40" style="margin-top:-5px;"/><?= Yii::$app->name ?></span>
                </a>
            </div>
            
            <p class="login-box-msg text-left"><?= Yii::t('app', $this->title); ?></p>
            <br>
            <br>
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>
            <div class="row">
                <div class="col-xs-12">
                    <?= $form
                        ->field($model, 'username', $fieldOptions1)
                        ->label(false)
                        ->textInput(['placeholder' => Yii::t('app', 'Username')]) ?>
                </div>
                <div class="col-xs-12">
                    <?= $form
                        ->field($model, 'password', $fieldOptions2)
                        ->label(false)
                        ->passwordInput(['placeholder' => Yii::t('app', 'Password')]) ?>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-7">
                    <?= $form->field($model, 'rememberMe')->label(Yii::t('app', "Remember me"))->checkbox() ?>
                </div>
                <!-- /.col -->
                <div class="col-xs-5">
                    <?= Html::submitButton(Yii::t('app', $this->title), ['class' => 'btn btn-red btn-block btn-flat', 'name' => 'login-button']) ?>
                </div>
                <!-- /.col -->
            </div>
            <?php ActiveForm::end(); ?>
            <?php echo Html::a(Yii::t('app', "Forgot your password?"), Url::to(['admin/user/request-password-reset'])) ?>
            <!-- /.lenguage -->
            <br><br>
            <div class="row">
                <div class="col-xs-12 col-md-offset-6 col-md-4 pull-right">
                    
                    <?= Select2::widget([
                            'name' => 'language',
                            'attribute' => 'language',
                            'id' => 'lang',
                            'hideSearch' => true,
                            'pluginEvents' => [
                                "change" => "function() {
                                                var data = new Array();    
                                                data.push({name: 'language', value: $(this).val()});
                                                $.ajax({
                                                    type: 'POST',
                                                    data: data,
                                                    url: '".Url::toRoute(['site/language'])."',
                                                    success: function () {
                                                    }
                                                });
                                            }",
                            ],
                            'data' => ['en' => 'English', 'es' => 'Español',],
                            'value' => Yii::$app->language,
                        ]);
                    ?>
                </div>
            </div><!-- /.lenguage -->
        </div> <!-- /.login-box-body -->

    </div> <!-- /.login-box -->
</div>
