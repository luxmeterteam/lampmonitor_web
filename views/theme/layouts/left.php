<?php
use yii\bootstrap\Nav;
use yii\widgets\Menu;
use app\models\UserEmpresa;
use app\models\UserCentro;

?>
<aside class="main-sidebar">

    <section class="sidebar p-t-15">

        <!-- Sidebar user panel
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php // $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div> -->

        <!-- search form 
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
         /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => Yii::t('app', 'Fish Farm'), 'icon' => 'map', 'url' => ['/centro/detallecentro'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->can('admin-centro')],
                    ['label' => Yii::t('app', 'Fish Farms'), 'icon' => 'map', 'url' => ['/centro/indexempresa'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->can('admin-empresa')],
                    ['label' => Yii::t('app', 'Companies'), 'icon' => 'industry', 'url' => ['/empresa/indexempresa'], 'visible' => !Yii::$app->user->isGuest && (Yii::$app->user->can('admin') || Yii::$app->user->can('admin-luxmeter'))],
                    [
                        'label' => Yii::t('app', 'Settings'),
                        'visible' => !Yii::$app->user->isGuest && Yii::$app->user->can('admin'),
                        'icon' => 'cogs',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('app', 'Users'), 'icon' => 'users', 'url' => ['/user/index'],],
                            ['label' => Yii::t('app', 'Companies'), 'icon' => 'industry', 'url' => ['/empresa/index'],],
                        ],
                    ],
                ],
            ]
        ) ?>

        <!--
        <ul class="sidebar-menu">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-share"></i> <span>Same tools</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php // \yii\helpers\Url::to(['/gii']) ?>"><span class="fa fa-file-code-o"></span> Gii</a>
                    </li>
                    <li><a href="<?php // \yii\helpers\Url::to(['/debug']) ?>"><span class="fa fa-dashboard"></span> Debug</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul> -->
        <?php
            if ( Yii::$app->user->can('admin-empresa') || Yii::$app->user->can('admin-centro') ) {
                if ( Yii::$app->user->can('admin-empresa') ) {
                    $user_empresa = UserEmpresa::find()->where([ 'id_user' => Yii::$app->user->identity->ID ])->one();
        
                    if ($user_empresa) {
                        $url_img = $user_empresa->empresa->url_imagen;
                    }
                    
                } else{
                    $user_centro = UserCentro::find()->where([ 'id_user' => Yii::$app->user->identity->ID ])->one();
        
                    if ($user_centro) {
                        $url_img = $user_centro->centro->empresa->url_imagen;
                    }
                } ?>
                <img  style="bottom: 20px; position:fixed; left:10px;" src="<?= Yii::$app->request->baseUrl.$url_img ?>" width="200px">
        <?php
            }
        ?>
        
    </section>
</aside>
