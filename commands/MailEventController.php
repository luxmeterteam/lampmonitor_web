<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

use app\models\Centro;
use app\models\CentroEmailNotification;
use app\models\User;
use app\models\UserCentro;
use app\models\UserEmpresa;
use app\models\UserNotification;
use app\models\LecturaLed;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Zulma Hernández
 * @since 2.0
 */
class MailEventController extends Controller
{
    /**
     * Envío de correos segun los eventos ocurridos por hora
     * @return int Exit code
     */
    public function actionIndex()
    {
        // Busco todos los centros
        $model_centros = Centro::find()->all();
        $time_inicial = date('H:i:s', strtotime('-360 minute'));
        $time_actual = date('H:i:s');
        $date_actual = date('Y-m-d');

        echo("El Time Inicial: " . $time_inicial);
        echo("El Time Actual: " . $time_actual);
        echo("El Date Actual: " . $date_actual);

        foreach ($model_centros as $key => $centro) {
            // Obtengo el ultimo estado por lampara por jaula por modulo
            $ultimos_estados_centro = Yii::$app->db->createCommand("
                SELECT id, lampara, fecha_lectura, hora_lectura, id_estado, jaula
                    FROM lm_lectura_led 
                    WHERE
                        id_centro=".$centro->id."
                        AND fecha_lectura = '".$date_actual."'
                        AND hora_lectura >= '".$time_inicial."'
                        AND hora_lectura <= '".$time_actual."'
                    ORDER BY jaula, lampara
            ")->queryAll();

            echo "<pre>";
            print_r($ultimos_estados_centro);
            echo "</pre>";

            die();

            // Si existen ultimos estados en el centro
            if (count($ultimos_estados_centro) > 0) {

                $modulos = 1; // Modulos por centro
                $jaulas = LecturaLed::find()->select('jaula')->distinct()->where(['id_centro' => $centro->id])->count(); // Jaulas por modulo
                //$lamparas = 6; // Lamparas por jaula
                $lamparas = LecturaLed::find()->select('lampara')->distinct()->where(['id_centro' => $centro->id])->count();
                $count_eventos = LecturaLed::find()->select('id_estado')->distinct()->where(['id_centro' => $centro->id])->count();
                $param_encendidas = $jaulas * $lamparas;
                $lecturas_fallas = [];
                $param_estados = 0;
                $lamps = $subject = '';
                $mail = true;

                foreach ($ultimos_estados_centro as $key => $lectura) {
                    if($lectura['id_estado'] == 1 || $lectura['id_estado'] == 4){
                        $param_estados += 1;
                    } elseif($lectura['id_estado'] == 3){
                        $param_estados += 2;
                    } else{
                        $lecturas_fallas[] = $lectura;
                    }
                }

                // Correos a notificar
                /*$ids_users_centro = ArrayHelper::getColumn(UserCentro::find()->select( 'id_user' )->where([ 'id_centro' => $centro->id ])->asArray()->all(), 'id_user');
                $ids_users_empresa = ArrayHelper::getColumn(UserEmpresa::find()->select( 'id_user' )->where([ 'id_empresa' => $centro->empresa->id ])->asArray()->all(), 'id_user');
                $ids_users = array_merge($ids_users_centro, $ids_users_empresa);
                $emails = ArrayHelper::getColumn(User::find()->where([ 'id' => $ids_users ])->asArray()->all(), 'email');*/
                $model_centro_email = CentroEmailNotification::find()->where(['id_centro' => $centro->id])->all();
                $emails =  ArrayHelper::getColumn($model_centro_email, 'email');

                // Obtengo las ultimas notificaciones del centro
                $ultima_notificacion_centro = Yii::$app->db->createCommand("SELECT * FROM lm_user_notification WHERE id_centro=".$centro->id." ORDER BY id DESC")->queryOne();

                // Cuando estan todas encendidas param_estados = jaulas * lamparas
                // Cuando estan todas sin comunicacion param_estados = jaulas * lamparas * 2
                // Cuando estan todas apagadas param_estados = 0
                // Cuando existe una falla (solo apagadas) cuando param_estados < encendidas
                // Tipos de notificaciones 0-Apagado, 1-Encendido, 2-Falla Lámpara
                if ($param_estados == $param_encendidas) { /* Encendido de lámparas */
                    if (!isset($ultima_notificacion_centro) || $ultima_notificacion_centro['type'] != 1) {
                        //Content mail
                        $subject = '['.$centro->nombre.'] Lamps On';
                        $pre_content = '<p>'.Yii::t('app', 'Lamps On').'</p>';
                        $content ='<p> Admin center '.$centro->nombre.', the lamps have lit.</p>';

                        // Tipo de notificación
                        $type = 1;
                    } else{
                        $mail = false;
                    }
                } elseif ($param_estados == 0) { /* Apagado de lámparas */
                    if (!isset($ultima_notificacion_centro) || $ultima_notificacion_centro['type'] != 0) {
                        //Content mail
                        $subject = '['.$centro->nombre.'] Lamps Off';
                        $pre_content = '<p>'.Yii::t('app', 'Lamps Off').'</p>';
                        $content ='<p> Admin center '.$centro->nombre.', the lamps have gone out.</p>';

                        // Tipo de notificación
                        $type = 0;
                    } else{
                        $mail = false;
                    }
                } elseif ($param_estados < $param_encendidas) { /* Error de lámparas */
                    if (!isset($ultima_notificacion_centro) || $ultima_notificacion_centro['type'] != 2) {
                        //Content mail
                        $subject = '['.$centro->nombre.'] Lamps Error';
                        $pre_content = '<p>'.Yii::t('app', 'Lamps Error').'</p>';
                        $content = '<p>Admin center '.$centro->nombre.', the following lamps are failing: <br> <ul>';
                        
                        $lamps = '';
                        foreach ($lecturas_fallas as $key => $lectura) {
                            $lamps .= 'C'.$lectura['jaula'].$lectura['lampara'].",";
                            $content .= '<li>Cage: '.$lectura['jaula'].' Lamp: '.$lectura['lampara'].' </li>';
                        }
                        $content .= '</ul></p>';
                        
                        // Tipo de notificación
                        $type = 2;
                    } else{
                        $mail = false;
                    }
                }

                //Enviar mail
                if (count($emails) > 0 && $mail) {

                    try {
                        Yii::$app->mailer->compose(['html'=>'layout'], ['pre_content' => $pre_content, 'content'=> $content])
                            ->setFrom([Yii::$app->params['adminEmail'] => 'LampMonitor'])
                            ->setTo($emails)
                            ->setSubject(Yii::t('app', $subject))
                            ->send();

                        //Guardar la notificacion
                        foreach ($model_centro_email as $key => $centro_email) {
                            $model_user_notification = new UserNotification();
                            $model_user_notification->date = date('Y-m-d H:i:s');
                            $model_user_notification->type = $type;
                            if ($type == 2) {
                                $lamps = substr($lamps, 0, -1);
                                $model_user_notification->cant_lamps = count($lecturas_fallas);
                                $model_user_notification->lamps = $lamps;
                            }
                            $model_user_notification->id_centro_email = $centro_email->id;
                            $model_user_notification->id_centro = $centro->id;
                            $model_user_notification->save(false);
                            unset($model_user_notification);
                        }
                    } catch (\yii\db\Exception $e) {
                        \Yii::$app->getSession()->setFlash('error', 'Error');
                    }
                }
            }
        }

        return ExitCode::OK;
    }
}