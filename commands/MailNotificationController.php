<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

use app\models\Centro;
use app\models\CentroEmailNotification;
use app\models\User;
use app\models\UserCentro;
use app\models\UserEmpresa;
use app\models\UserNotification;
use app\models\LecturaLed;
use app\models\EventCentro;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Zulma Hernández
 * @since 2.0
 */
class MailNotificationController extends Controller
{
    /**
     * @return int Exit code
     */
    public function actionIndex()
    {
        // Busco todos los centros
        $model_centros = Centro::find()->all();

        foreach ($model_centros as $key => $centro) {
            // Obtengo el ultimo estado por lampara por jaula por modulo
            $ultimos_estados_centro = Yii::$app->db->createCommand("
                SELECT id, lampara, fecha_lectura, hora_lectura, id_estado, jaula
                    FROM lm_lectura_led 
                    WHERE 
                        id IN (SELECT max(id) FROM lm_lectura_led WHERE id_centro=".$centro->id." GROUP BY jaula, lampara)
                    ORDER BY jaula, lampara
            ")->queryAll();

            // Si existen ultimos estados en el centro
            if (count($ultimos_estados_centro) > 0) {

                $modulos = 1; // Modulos por centro
                $jaulas = LecturaLed::find()->select('jaula')->distinct()->where(['id_centro' => $centro->id])->count(); // Jaulas por modulo
                /*echo "Centro: " . $centro->id;
                echo " jaulas: " . $jaulas;*/
                //$lamparas = 6; // Lamparas por jaula
                $lamparas = LecturaLed::find()->select('lampara')->distinct()->where(['id_centro' => $centro->id])->count();
                //echo "lamparas: " . $lamparas;
                $param_encendidas = $jaulas * $lamparas;
                $lecturas_fallas = $lecturas_not = [];
                $param_estados = 0;
                $lamps = $subject = '';
                $mail = true;
                $event = false;
                $lamps_on = $lamps_off = $lamps_not = $lamps_reconnected = 0;

                foreach ($ultimos_estados_centro as $key => $lectura) {

                    if ($lectura['id_estado'] == 1) { //Lamps ON
                        $lamps_on += 1;
                    } elseif ($lectura['id_estado'] == 2) { //Lamps OFF
                        $lamps_off += 1;
                        $lecturas_fallas[] = $lectura;
                    } elseif ($lectura['id_estado'] == 3) { //Lamps NOT COMMUNICATION
                        $lamps_not += 1;
                        $lecturas_not[] = $lectura;
                    } elseif ($lectura['id_estado'] == 4) { //Lamps RECONNECTED
                        $estatus_temporal = 0;
                        $estados_led_reconectado = Yii::$app->db->createCommand("
                            SELECT lampara, fecha_lectura, hora_lectura, id_estado
                                FROM lm_lectura_led 
                                WHERE jaula='".$lectura['jaula']."' AND lampara=".$lectura['lampara']." 
                                    AND id_estado != 4 AND id_estado != 3
                                ORDER BY id DESC
                                LIMIT 1
                        ")->queryOne();

                        $estatus_temporal = $estados_led_reconectado["id_estado"];

                        if ($estatus_temporal == 1) {
                            $lamps_on += 1;
                        } elseif ($estatus_temporal == 2) {
                            $lamps_off += 1;
                            $lecturas_fallas[] = $lectura;
                        }
                    }


                    /*if($lectura['id_estado'] == 1 || $lectura['id_estado'] == 4){
                        $param_estados += 1;
                    } elseif($lectura['id_estado'] == 3){
                        $param_estados += 2;
                    } else{
                        $lecturas_fallas[] = $lectura;
                    }*/
                }

                /*echo "Lamps ON: " . $lamps_on;
                echo "Lamps OFF: " . $lamps_off;
                echo "Lamps NOT: " . $lamps_not;
                echo "Encendidas: " . $param_encendidas;*/

                // Correos a notificar
                /*$ids_users_centro = ArrayHelper::getColumn(UserCentro::find()->select( 'id_user' )->where([ 'id_centro' => $centro->id ])->asArray()->all(), 'id_user');
                $ids_users_empresa = ArrayHelper::getColumn(UserEmpresa::find()->select( 'id_user' )->where([ 'id_empresa' => $centro->empresa->id ])->asArray()->all(), 'id_user');
                $ids_users = array_merge($ids_users_centro, $ids_users_empresa);
                $emails = ArrayHelper::getColumn(User::find()->where([ 'id' => $ids_users ])->asArray()->all(), 'email');*/
                $model_centro_email = CentroEmailNotification::find()->where(['id_centro' => $centro->id])->all();
                $emails = ArrayHelper::getColumn($model_centro_email, 'email');

                // Obtengo las ultimas notificaciones del centro
                $ultima_notificacion_centro = Yii::$app->db->createCommand("SELECT * FROM lm_user_notification WHERE id_centro=".$centro->id." ORDER BY id DESC")->queryOne();

                // Obtengo el estatus anterior almacenado
                $model_event_centro = EventCentro::findOne(['id_centro' => $centro->id]);

                if (isset($model_event_centro) && $model_event_centro->cant_lamps_on == $lamps_on && $model_event_centro->cant_lamps_off == $lamps_off && $model_event_centro->cant_lamps_reconnected == $lamps_reconnected && $model_event_centro->cant_lamps_not == $lamps_not) {
                    $event = false;
                } else{
                    if(!isset($model_event_centro)){
                        $model_event_centro = new EventCentro();
                        $model_event_centro->id_centro = $centro->id;
                    }
                        
                    $event = true;
                }

                // Cuando estan todas encendidas param_estados = jaulas * lamparas
                // Cuando estan todas sin comunicacion param_estados = jaulas * lamparas * 2
                // Cuando estan todas apagadas param_estados = 0
                // Cuando existe una falla (solo apagadas) cuando param_estados < encendidas
                // Tipos de notificaciones 0-Apagado, 1-Encendido, 2-Falla Lámpara
                if ( ($lamps_on == $param_encendidas) || ($lamps_reconnected == $param_encendidas) ) { /* Encendido de lámparas */
                    echo "Entro en la primera validación";
                    if (!isset($ultima_notificacion_centro) || $ultima_notificacion_centro['type'] != 1) {
                        //Content mail
                        $subject = '['.$centro->nombre.'] Lamps On';
                        $pre_content = '<p>'.Yii::t('app', 'Lamps On').'</p>';
                        $content ='<p> Admin center '.$centro->nombre.', the lamps have lit.</p>';

                        // Tipo de notificación
                        $type = 1;

                        //Enventos
                        if ($event) {
                            $model_event_centro->index = 1;
                        }
                    } else{
                        $mail = false;
                    }
                } elseif ( ($lamps_off == $param_encendidas) || ($lamps_not == $param_encendidas) ) { /* Apagado de lámparas */
                    echo "Entro en la segunda validación";
                    if (!isset($ultima_notificacion_centro) || $ultima_notificacion_centro['type'] != 0) {
                        //Content mail
                        $subject = '['.$centro->nombre.'] Lamps Off';
                        $pre_content = '<p>'.Yii::t('app', 'Lamps Off').'</p>';
                        $content ='<p> Admin center '.$centro->nombre.', the lamps have gone out.</p>';

                        // Tipo de notificación
                        $type = 0;

                        //Enventos
                        if ($event) {
                            $model_event_centro->index = 1;
                        }
                    } else{
                        $mail = false;
                    }
                } elseif ( (($lamps_off > 0 && $lamps_off < $param_encendidas) && ($lamps_not > 0 && $lamps_not < $param_encendidas)) ) { /* Apagado de lámparas */
                    echo "Entro en la tercera validación";
                    if ($event) {
                        $model_event_centro->index = 1;
                        $mail = false;
                    } elseif (!$event && $model_event_centro->index >= 1 && $model_event_centro->index <= 2){
                        $model_event_centro->index += 1;
                        $mail = false;
                    } elseif (!$event && $model_event_centro->index == 3 && $model_event_centro->mail_send == 0){
                        if (!isset($ultima_notificacion_centro) || $ultima_notificacion_centro['type'] != 2) {
                            //Content mail
                            $subject = '['.$centro->nombre.'] Lamps Error';
                            $pre_content = '<p>'.Yii::t('app', 'Lamps Error').'</p>';
                            $content = '<p>Admin center '.$centro->nombre.', the following lamps are failing: <br> <ul>';
                            
                            $lamps = '';
                            foreach ($lecturas_fallas as $key => $lectura) {
                                $lamps .= 'C'.$lectura['jaula'].$lectura['lampara'].",";
                                $content .= '<li>Cage: '.$lectura['jaula'].' Lamp: '.$lectura['lampara'].' </li>';
                            }
                            $content .= '</ul>
                            <br><br>
                            And the following lamps are without communication: <br> <ul>';
                            
                            foreach ($lecturas_not as $key => $lectura) {
                                $lamps .= 'C'.$lectura['jaula'].$lectura['lampara'].",";
                                $content .= '<li>Cage: '.$lectura['jaula'].' Lamp: '.$lectura['lampara'].' </li>';
                            }

                            $content .= '</ul></p>';
                            
                            // Tipo de notificación
                            $type = 2;
                            $mail = true;
                        } else{
                            $mail = false;
                        }
                    } else{
                        $mail = false;
                    }
                } elseif ( ($lamps_off > 0 && $lamps_off < $param_encendidas && ($lamps_on > 0 && $lamps_on < $param_encendidas || $lamps_reconnected > 0 && $lamps_reconnected < $param_encendidas)) && (($lamps_off + $lamps_on) == $param_encendidas || ($lamps_off + $lamps_reconnected) == $param_encendidas) ) { /* Error de lámparas */
                    echo "Entro en la cuarta validación";
                    if ($event) {
                        $model_event_centro->index = 1;
                        $mail = false;
                    } elseif (!$event && $model_event_centro->index >= 1 && $model_event_centro->index <= 2){
                        $model_event_centro->index += 1;
                        $mail = false;
                    } elseif (!$event && $model_event_centro->index == 3 && $model_event_centro->mail_send == 0){
                        if (!isset($ultima_notificacion_centro) || $ultima_notificacion_centro['type'] != 2) {
                            //Content mail
                            $subject = '['.$centro->nombre.'] Lamps Error';
                            $pre_content = '<p>'.Yii::t('app', 'Lamps Error').'</p>';
                            $content = '<p>Admin center '.$centro->nombre.', the following lamps are failing: <br> <ul>';
                            
                            $lamps = '';
                            foreach ($lecturas_fallas as $key => $lectura) {
                                $lamps .= 'C'.$lectura['jaula'].$lectura['lampara'].",";
                                $content .= '<li>Cage: '.$lectura['jaula'].' Lamp: '.$lectura['lampara'].' </li>';
                            }
                            $content .= '</ul></p>';
                            
                            // Tipo de notificación
                            $type = 2;
                            $mail = true;
                        } else{
                            $mail = false;
                        }
                    } else{
                        $mail = false;
                    }
                } elseif ( ($lamps_not > 0 && $lamps_not < $param_encendidas && ($lamps_on > 0 && $lamps_on < $param_encendidas || $lamps_reconnected > 0 && $lamps_reconnected < $param_encendidas)) && (($lamps_not + $lamps_on) == $param_encendidas || ($lamps_not + $lamps_reconnected) == $param_encendidas) ) { /* Error de lámparas */
                    echo "Entro en la quinta validación";
                    if ($event) {
                        $model_event_centro->index = 1;
                        $mail = false;
                    } elseif (!$event && $model_event_centro->index >= 1 && $model_event_centro->index <= 2){
                        $model_event_centro->index += 1;
                        $mail = false;
                    } elseif (!$event && $model_event_centro->index == 3 && $model_event_centro->mail_send == 0){
                        if (!isset($ultima_notificacion_centro) || $ultima_notificacion_centro['type'] != 2) {
                            //Content mail
                            $subject = '['.$centro->nombre.'] Lamps Error';
                            $pre_content = '<p>'.Yii::t('app', 'Lamps Error').'</p>';
                            $content = '<p>Admin center '.$centro->nombre.', the following lamps are without communication: <br> <ul>';
                            
                            $lamps = '';
                            foreach ($lecturas_not as $key => $lectura) {
                                $lamps .= 'C'.$lectura['jaula'].$lectura['lampara'].",";
                                $content .= '<li>Cage: '.$lectura['jaula'].' Lamp: '.$lectura['lampara'].' </li>';
                            }
                            $content .= '</ul></p>';
                            
                            // Tipo de notificación
                            $type = 2;
                            $mail = true;
                        } else{
                            $mail = false;
                        }
                    } else{
                        $mail = false;
                    }
                }

                //Enviar mail
                if (count($emails) > 0 && $mail) {

                    try {
                        Yii::$app->mailer->compose(['html'=>'layout'], ['pre_content' => $pre_content, 'content'=> $content])
                            ->setFrom([Yii::$app->params['adminEmail'] => 'LampMonitor'])
                            ->setTo($emails)
                            ->setSubject(Yii::t('app', $subject))
                            ->send();

                        //Guardar la notificacion
                        if($type == 2)
                            $lamps = substr($lamps, 0, -1);

                        foreach ($model_centro_email as $key => $centro_email) {
                            $model_user_notification = new UserNotification();
                            $model_user_notification->date = date('Y-m-d H:i:s');
                            $model_user_notification->type = $type;
                            if ($type == 2) {
                                $model_user_notification->cant_lamps = count($lecturas_fallas);
                                $model_user_notification->lamps = $lamps;
                            }
                            $model_user_notification->id_centro_email = $centro_email->id;
                            $model_user_notification->id_centro = $centro->id;
                            $model_user_notification->save(false);
                            unset($model_user_notification);
                        }
                    } catch (\yii\db\Exception $e) {
                        \Yii::$app->getSession()->setFlash('error', 'Error');
                    }
                }

                //Guardar el evento
                if($event) {
                    $model_event_centro->index = 1;
                    $model_event_centro->cant_lamps_on = $lamps_on;
                    $model_event_centro->cant_lamps_off = $lamps_off;
                    $model_event_centro->cant_lamps_reconnected = $lamps_reconnected;
                    $model_event_centro->cant_lamps_not = $lamps_not;
                    $model_event_centro->cant_lamps = $param_encendidas;
                }

                $model_event_centro->mail_send = $mail;
                $model_event_centro->date = date('Y-m-d H:i:s');
                $model_event_centro->save(false);
            }
        }

        return ExitCode::OK;
    }
}
