<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=ledmonitor',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

    /*'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=200.24.235.224;dbname=lampmonitor;',
    'username' => 'luxmet22',
    'password' => 'mxO8skEnHqvz1gof',
    'charset' => 'utf8',*/

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];