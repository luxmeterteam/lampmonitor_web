<?php

return [
    'adminEmail' => 'noreplyluxmeter@gmail.com',
    'senderEmail' => 'noreplyluxmeter@gmail.com',
    'senderName' => 'noreplyluxmeter@gmail.com',
    'supportEmail' => 'noreplyluxmeter@gmail.com',
    'languages' => [
        'en'=>'English',
        'es'=>'Español'    
    ], 
];
