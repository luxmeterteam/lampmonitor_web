<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

use app\models\User;
use app\models\UserSearch;
use app\models\AuthItem;
use app\models\AuthAssignment;
use app\models\Centro;
use app\models\CentroEmailNotification;
use app\models\Empresa;
use app\models\UserEmpresa;
use app\models\UserCentro;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public $basePath ='/uploads';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $filter_roles = ArrayHelper::map(AuthItem::find()->where(['type' => 1])->all(), 'name', 
                                    function($model){
                                        return ucfirst($model->name);
                                    });

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'filter_roles' => $filter_roles,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        //Listado de Roles
        $model_item = ArrayHelper::map(AuthItem::find()->where(['type' => 1])->all(), 'name', 
                                    function($model){
                                        return ucfirst($model->name);
                                    });

        //Empresas
        $model_user_empresa = new UserEmpresa();
        $empresas = ArrayHelper::map(Empresa::find()->all(), 'id', 'nombre');

        //Centros
        $model_user_centro = new UserCentro();
        $centros = ArrayHelper::map(Centro::find()->all(), 'id', 'nombre');

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) ) {
            $model->setPassword('123456');
            $model->generateAuthKey();
            $model->generatePasswordResetToken();
            $model->created_at = date('Y-m-d H:i:s');
            $model->updated_at = date('Y-m-d H:i:s');

            if ($model->save(false)) {
                $model_assignment = new AuthAssignment();
                $model_assignment->item_name = $model->rol;
                $model_assignment->user_id   = $model->id;
                $model_assignment->save(false);

                if ( $model->rol === 'admin-empresa' || $model->rol === 'admin-centro') {
                    $cargo = '';
                    if ( $model->rol === 'admin-empresa' ) {
                        $user_empresa = Yii::$app->request->post('UserEmpresa');
    
                        if ( isset($user_empresa['id_empresa']) && $user_empresa['id_empresa'] != '') {
                            $model_user_empresa = new UserEmpresa();
                            $model_user_empresa->id_empresa = $user_empresa['id_empresa'];
                            $model_user_empresa->id_user = $model->id;
                            $model_user_empresa->save(false);
                        }
                        
                        $cargo = "Company Manager";
                        $ids_centros = ArrayHelper::getColumn(Centro::find()->select( 'id' )->where([ 'id_empresa' => $user_empresa['id_empresa'] ])->asArray()->all(), 'id');
                        foreach ($ids_centros as $key => $id_centro) {
                            $model_centro_email = new CentroEmailNotification();
                            $model_centro_email->cargo = $cargo;
                            $model_centro_email->email = $model->email;
                            $model_centro_email->id_user = $model->id;
                            $model_centro_email->id_centro = $id_centro;
                            $model_centro_email->date = date('Y-m-d H:i:s');
                            $model_centro_email->save(false);
                        }
                    } elseif ( $model->rol === 'admin-centro' ) {
                        $user_centro = Yii::$app->request->post('UserCentro');
    
                        if ( isset($user_centro['id_centro']) && $user_centro['id_centro'] != '') {
                            $model_user_centro = new UserCentro();
                            $model_user_centro->id_centro = $user_centro['id_centro'];
                            $model_user_centro->id_user = $model->id;
                            $model_user_centro->save(false);
                        }

                        $cargo = "Center Manager";
                        $model_centro_email = new CentroEmailNotification();
                        $model_centro_email->cargo = $cargo;
                        $model_centro_email->email = $model->email;
                        $model_centro_email->id_user = $model->id;
                        $model_centro_email->id_centro = $user_centro['id_centro'];
                        $model_centro_email->date = date('Y-m-d H:i:s');
                        $model_centro_email->save(false);
                    }
                }
            }

            try {
                //Email
                $pre_content = '<p>'.Yii::t('app', 'Welcome to LampMonitor!').'</p>';
                $content =
                    '<p>
                        <b>'.Yii::t('app', 'Username').':</b> ' . $model->username . '<br>
                        <b>'.Yii::t('app', 'Password').':</b> ' . $model->pass_aux . '<br>
                    </p>';
                Yii::$app->mailer->compose(['html'=>'layout'], ['pre_content' => $pre_content, 'content'=> $content])
                    ->setFrom([Yii::$app->params['adminEmail'] => 'LampMonitor'])
                    ->setTo($model->email)
                    ->setSubject(Yii::t('app', 'New LampMonitor Account'))
                    ->send();

            } catch (\yii\db\Exception $e) {
                \Yii::$app->getSession()->setFlash('error', 'Error');
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'model_item' => $model_item,
            'model_user_empresa' => $model_user_empresa,
            'empresas' => $empresas,
            'model_user_centro' => $model_user_centro,
            'centros' => $centros,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->rol = AuthAssignment::findOne(['user_id'=>$model->id])->item_name;
        
        //Listado de Roles
        $model_item = ArrayHelper::map(AuthItem::find()->where(['type' => 1])->all(), 'name', 
                                    function($model){
                                        return ucfirst($model->name);
                                    });
        
        //Empresa
        $model_user_empresa = new UserEmpresa();
        $empresas = ArrayHelper::map(Empresa::find()->all(), 'id', 'nombre');

        //Centro
        $model_user_centro = new UserCentro();
        $centros = ArrayHelper::map(Centro::find()->all(), 'id', 'nombre');
        
        if ($model->rol == 'admin-empresa') {
            $empresa_reg = UserEmpresa::find()->select('id_empresa')->where(['id_user' => $model->id])->one();
            $model_user_empresa->id_empresa = $empresa_reg->id_empresa;
        } elseif ( $model->rol == 'admin-centro' ) {
            $centro_reg = UserCentro::find()->where(['id_user' => $model->id])->one();
            $model_user_centro->id_centro = $centro_reg->id_centro;
            $model_user_centro->id_empresa = $centro_reg->centro->id_empresa;
        }
        
        //Si el rol registrado cambia se debe eliminar el rol anterior y registrar el nuevo rol
        //Si cambia la empresa se debe eliminar la empresa de la tabla de asignacion de usuarios y registrar la nueva
        //Si cambia el centro se debe eliminar el centro de la tabla de asignacion de usuarios y registrar el nuevo centro
        //Actualizar el correo de las notificaciones para el centro
        if ($model->load(Yii::$app->request->post())) {

            if ($model->newPassword) {
                $model->setPassword($model->newPassword);
                $model->generateAuthKey();

                try {
                    //Email
                    $pre_content = '<p>'.Yii::t('app', 'Your password has been reset').'</p>';
                    $content =
                        '<p>
                            <b>'.Yii::t('app', 'Username').':</b> ' . $model->username . '<br>
                            <b>'.Yii::t('app', 'Password').':</b> ' . $model->pass_aux . '<br>
                        </p>';

                    Yii::$app->mailer->compose(['html'=>'layout'], ['pre_content' => $pre_content, 'content'=> $content])
                        ->setFrom([Yii::$app->params['adminEmail'] => 'LampMonitor'])
                        ->setTo($model->email)
                        ->setSubject(Yii::t('app', 'Password Reset By Admin'))
                        ->send();
    
                } catch (\yii\db\Exception $e) {
                    \Yii::$app->getSession()->setFlash('error', 'Error');
                }
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'model_item' => $model_item,
            'model_user_empresa' => $model_user_empresa,
            'empresas' => $empresas,
            'model_user_centro' => $model_user_centro,
            'centros' => $centros,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetcentros() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            
            if ( $parents != null ) {
                $id_empresa = $parents[0];
                $selected = null;
                $out = Centro::find()->select(['id' => 'id','name' =>'nombre' ])->where(['id_empresa' => $id_empresa])->orderBy('nombre')->asArray()->all();  
                
                return Json::encode(['output'=>$out, 'selected'=>$selected]);
            }
        }
        return Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function actionForgot(){
        $model = new User();

        if ($model->load(Yii::$app->request->post())) {
            if(isset($model->email)){
                $user = User::findOne(["email" => $model->email]);
                /*echo "<pre>";
                print_r($user);
                echo "</pre>";*/
            }
        }

        return $this->render('forgot', [
            'model' => $model,
        ]);
    }

    /**
     * User Profile
     * @return mixed
     */
    public function actionProfile($id)
    {
        $model = $this->findModel($id);

        $prefixes = ['+56' => '+56', '+47' => '+47'];

        if ($model->telefono) {
            $model->prefix = substr($model->telefono, 0, 3);
            $model->telefono = substr($model->telefono, 3);
        }

        if ($model->load(Yii::$app->request->post())) {

            $model->updated_at = date('Y-m-d H:i:s');
            if ($model->telefono != '') {
                $model->telefono = Yii::$app->request->post('User')['prefix'].$model->telefono;
            }

            if ($model->save(false)) {

                $model->image = \yii\web\UploadedFile::getInstance($model, "image");

                //Crear carpeta Empresa
                $path = $this->basePath .'/usuarios/'. $model->id . '/';
                if ( !is_dir(".".$path) ) {
                    mkdir( ".".$path, 0777, true );
                }

                if ( !empty($model->image->extension)) {
                    $file_name = "profile_img_".$model->id.".{$model->image->extension}";

                    $path = $path . $file_name;
                    $model->url_imagen = $path;

                    if($model->save(false)){
                        $model->image->saveAs(".".$path);
                    }
                }

                return $this->redirect(['user/profile', 'id' => $model->id]);
            }
        }

        return $this->render('profile', [
            'model' => $model,
            'prefixes' => $prefixes,
        ]);
    }
}
