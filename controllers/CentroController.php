<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use app\models\Centro;
use app\models\CentroSearch;
use app\models\UserCentro;
use app\models\UserEmpresa;
use app\models\BitacoraConexion;
use app\models\CentroEmailNotification;
use app\models\LecturaLed;
use app\models\LecturaLedSearch;
use app\models\Model;

/**
 * CentroController implements the CRUD actions for Centro model.
 */
class CentroController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Centro models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CentroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Centro model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Centro model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Centro();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Centro model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Centro model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Centro model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Centro the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Centro::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Listado de centros de una empresa.
     * @return mixed
     */
    public function actionIndexempresa($id = 0)
    {
        if (Yii::$app->user->can('admin-empresa')) {
            //Obtengo la(s) empresas asociadas al usuario
            $id = UserEmpresa::find()->select( 'id_empresa' )->where([ 'id_user' => Yii::$app->user->identity->ID ])->all();
        }

        if ( isset($id) && $id != 0) {
            $model_centros = Centro::find()->where(['id_empresa' => $id])->all();

            //Obtener la ultima conexión de los centros
            $ids_centros =  ArrayHelper::getColumn($model_centros, 'id');

            $model_bitacora_conexion = BitacoraConexion::find()->where(['IN', 'id_centro', $ids_centros])->all();

            return $this->render('index-empresa', [
                'model_centros' => $model_centros,
                'model_bitacora_conexion' => $model_bitacora_conexion,
            ]);
        }
        
        return $this->redirect(['site/index']);
    }

    /**
     * Detalle de un centro
     * @return mixed
     */
    public function actionDetallecentro( $id = 0 )
    {
        if ( Yii::$app->user->can('admin-centro')) {
            //Se consulta el id del usuario admin-centro
            $user_centro = UserCentro::find()->select( 'id_centro' )->where([ 'id_user' => Yii::$app->user->identity->ID ])->one();
            $id = $user_centro->id_centro;
        }

        $model_centro = $this->findModel($id);
        $nombre_modulos = ArrayHelper::getColumn(LecturaLed::find()->select('modulo')->distinct()->where(['id_centro' => $model_centro->id])->all(), 'modulo'); // Modulos por centro

        $nombre_jaulas =  [];
        $modulos = (count($nombre_modulos) > 0)?count($nombre_modulos):1;
        $jaulas = 10;
        $tipo_jaula = 1;

        //Parametros de consulta de las lecturas
        $fecha_inicial = date('Y-m-d', strtotime('-30 days'));

        //Consultar el ultimo estatus por led en las lecturas
        $estatus_jaulas = $estatus_lamparas = $dataProvider = [];
        for ($i=0; $i < $modulos; $i++) {
            //Jaulas
            if (count($nombre_modulos) > 0) {
                $nombre_jaulas[] = ArrayHelper::getColumn(LecturaLed::find()->select('jaula')->distinct()->where(['id_centro' => $model_centro->id, 'modulo' => $nombre_modulos[$i]])->all(), 'jaula'); // Jaulas por modulo

                //Registros para el archivo excel
                $searchModel = new LecturaLedSearch(['id_centro' => $id, 'modulo' => $nombre_modulos[$i], 'fecha_lectura' => $fecha_inicial ]);
                $dataProvider[$i] = $searchModel->searchCentro(Yii::$app->request->queryParams);
            } else{
                //Registros para el archivo excel
                $searchModel = new LecturaLedSearch(['id_centro' => $id, 'fecha_lectura' => $fecha_inicial ]);
                $dataProvider[$i] = $searchModel->searchCentro(Yii::$app->request->queryParams);
            }

            for ($j=0; $j < $jaulas; $j++) {
                $jaula_consulta = (isset($nombre_modulos[$i][0])?$nombre_modulos[$i][0]:($j+1)).(($j+1 < 10)?"0":"").($j+1);

                // 1) Obtengo el ultimo estado por lampara por jaula por modulo
                $ultimas_estados_jaula = Yii::$app->db->createCommand("
                    SELECT id, lampara, fecha_lectura, hora_lectura, id_estado, jaula
                        FROM lm_lectura_led 
                        WHERE 
                            (id_centro, id_local) IN (SELECT id_centro, max(id_local) FROM lm_lectura_led WHERE jaula='".$jaula_consulta."' AND id_centro=".$model_centro->id.((count($nombre_modulos) > 0)?" AND modulo='".$nombre_modulos[$i]."'":"")." GROUP BY lampara)
                        ORDER BY lampara
                ")->queryAll();

                if (count($ultimas_estados_jaula) > 0) {
                    foreach ($ultimas_estados_jaula as $key => $lampara) {
                        if ($lampara["id_estado"] != 4 && $lampara["id_estado"] != 3) {
                            if (isset($estatus_jaulas[$i+1][$j])) {
                                if ($estatus_jaulas[$i+1][$j] != 2) {
                                    if ($lampara["id_estado"] == 2){
                                        $estatus_jaulas[$i+1][$j] = $lampara["id_estado"];
                                    } else{
                                        if ($estatus_jaulas[$i+1][$j] < $lampara["id_estado"]) {
                                            $estatus_jaulas[$i+1][$j] = $lampara["id_estado"];
                                        }
                                    }
                                }
                            } else{
                                $estatus_jaulas[$i+1][$j] = $lampara["id_estado"];
                            }
                            $estatus_lamparas[$i][$j][$lampara["lampara"]] = $lampara["id_estado"];
                        } else{
                            $estatus_temporal = 0;
                            $estados_led_reconectado = Yii::$app->db->createCommand("
                                SELECT lampara, fecha_lectura, hora_lectura, id_estado
                                    FROM lm_lectura_led 
                                    WHERE jaula='".$jaula_consulta."' AND lampara=".$lampara["lampara"]." 
                                        AND id_estado != 4 AND id_estado != 3 AND id_centro=".$model_centro->id." 
                                    ORDER BY id_local DESC
                                    LIMIT 1
                                ")->queryOne();
                            
                            $estatus_temporal = (!empty($estados_led_reconectado))?$estados_led_reconectado["id_estado"]:0;

                            if (isset($estatus_jaulas[$i+1][$j])) {
                                if ($estatus_jaulas[$i+1][$j] != $estatus_temporal) {
                                    if ($estatus_jaulas[$i+1][$j] != 2) {
                                        if ($estatus_jaulas[$i+1][$j] < $estatus_temporal) {
                                            $estatus_jaulas[$i+1][$j] = $estatus_temporal;
                                        }
                                    }
                                }
                            } else{
                                $estatus_jaulas[$i+1][$j] = $estatus_temporal;
                            }

                            $estatus_lamparas[$i][$j][$lampara["lampara"]] = $estatus_temporal;
                        }
                    }
                } else{
                    $estatus_jaulas[$i+1][$j] = 0;
                }
                
            }
        }

        //Ultima conexion centro
        $model_bitacora_conexion = BitacoraConexion::find()->where(['id_centro' => $id])->orderBy('ultima_conexion DESC')->one();

        return $this->render('detalle-centro', [
            'model_centro' => $model_centro,
            'modulos' => $modulos,
            'jaulas' => $jaulas,
            'tipo_jaula' => $tipo_jaula,
            'estatus_jaulas' => $estatus_jaulas,
            'estatus_lamparas' => $estatus_lamparas,
            'model_bitacora_conexion' => $model_bitacora_conexion,
            'dataProvider' => $dataProvider,
            'nombre_modulos' => $nombre_modulos,
            'nombre_jaulas' => $nombre_jaulas
        ]);
    }

    /**
     * Detalle de una jaula
     * @return mixed
     */
    public function actionDetallejaula( $id = 0, $id_centro = 0 )
    {
        //Centro
        $model_centro = $this->findModel($id_centro);

        //Parametros de consulta de las lecturas
        $fecha_inicial = date('Y-m-d', strtotime('-7 days'));

        //Cantidad de lamparas
        $lamparas = LecturaLed::find()->select('lampara')->distinct()->where(['id_centro' => $id_centro, 'jaula' => $id])->orderBy('lampara ASC')->all();

        //Numero de las lamparas
        $nlamparas = count($lamparas);

        //Lecturas
        $lecturas = LecturaLed::find()->where(['id_centro' => $id_centro, 'jaula' => $id])
                                      ->andWhere(['>=', 'fecha_lectura', $fecha_inicial])
                                      ->andWhere(['!=', 'id_estado', 3])
                                      ->andWhere(['!=', 'id_estado', 4])
                                      ->orderBy('lampara, fecha_lectura, hora_lectura ASC')
                                      ->all();
        
        $series = $dataProvider = [];
        if (count($lecturas) > 0) {
            //Lampara inicial
            $lampara_actual = $lecturas[0]->lampara;
            $nserie = $lecturas[0]->nserie;
            $fecha_actual = $lecturas[0]->fecha_lectura;
            $hora_actual = $lecturas[0]->hora_lectura;
            $estado_actual = $lecturas[0]->id_estado;
            $cant_lecturas = count($lecturas);
            $index = 0;
            $data = [];
            foreach ($lamparas as $key => $lamp) {
                $data = [];
                $ban = false;
                if ($lamp->lampara == $lampara_actual) {
                    foreach ($lecturas as $i => $lectura) {
                        $hora_for_actual = explode(":", $hora_actual);

                        if ($lectura->lampara == $lampara_actual) {
                            $hora_for = explode(":", $lectura->hora_lectura);
                            if ($fecha_actual == $lectura->fecha_lectura && $hora_actual < $lectura->hora_lectura) {
                                $diff = intval($hora_for[0]) - intval($hora_for_actual[0]);
                                if ($diff > 1) {
                                    for ($j=intval($hora_for_actual[0])+1; $j < intval($hora_for[0]) ; $j++) { 
                                        $data[] = [$lectura->fecha_lectura . ' ' . $j . ':00', (($estado_actual == 2)?1:2)];
                                    }
                                }
                            } elseif ($fecha_actual < $lectura->fecha_lectura) {
                                $fecha1 = new \DateTime($fecha_actual);
                                $fecha2 = new \DateTime($lectura->fecha_lectura);
                                $intervalo = $fecha2->diff($fecha1);
        
                                if (intval($intervalo->format('%a')) >= 1) {
                                    for ($j=intval($hora_for_actual[0])+1; $j < 24 ; $j++) { 
                                        $data[] = [$fecha_actual . ' ' . $j . ':00', (($estado_actual == 2)?1:2)];
                                    }
                                    if (intval($intervalo->format('%a')) > 1) {
                                        $aux_fecha = $fecha1;
                                        for ($k=1; $k < intval($intervalo->format('%a')) ; $k++) {
                                            $aux_fecha->add(new \DateInterval('P1D'));
                                            for ($j=0; $j < 24 ; $j++) { 
                                                $data[] = [$aux_fecha->format('Y-m-d') . ' ' . $j . ':00', (($estado_actual == 2)?1:2)];
                                            }
                                        }
                                    }
                                    for ($j=0; $j < intval($hora_for[0]) ; $j++) { 
                                        $data[] = [$lectura->fecha_lectura . ' ' . $j . ':00', (($estado_actual == 2)?1:2)];
                                    }
                                }
                                
                            }
                            $data[] = [$lectura->fecha_lectura . ' ' . $lectura->hora_lectura, (($lectura->id_estado == 2)?1:2)];
                            $ban = true;
                            //Para guardar el ultimo registro
                            if ( $i === ($cant_lecturas -1) ) {
                                $searchModel = new LecturaLedSearch(['id_centro' => $id_centro, 'jaula' => $id, 'lampara' => $lampara_actual, 'fecha_lectura' => $fecha_inicial ]);
                                $dataProvider[$index] = $searchModel->searchLeds(Yii::$app->request->queryParams);
            
                                $series[$index] = [
                                    [
                                        'name' => $nserie,
                                        'data' => $data,
                                    ],
                                ];
                            }
                            
                        } else {
                            if ($ban) {
                                $searchModel = new LecturaLedSearch(['id_centro' => $id_centro, 'jaula' => $id, 'lampara' => $lampara_actual, 'fecha_lectura' => $fecha_inicial ]);
                                $dataProvider[$index] = $searchModel->searchLeds(Yii::$app->request->queryParams);
            
                                $series[$index] = [
                                    [
                                        'name' => $nserie,
                                        'data' => $data,
                                    ],
                                ];
                                $lampara_actual = $lectura->lampara;
                                $nserie = $lectura->nserie;
                                $ban = false;
                                break;
                            } else {
                                continue;
                            }
                        }

                        $fecha_actual = $lectura->fecha_lectura;
                        $hora_actual = $lectura->hora_lectura;
                        $estado_actual = $lectura->id_estado;
                    }
                } else{
                    $searchModel = new LecturaLedSearch(['id_centro' => $id_centro, 'jaula' => $id, 'lampara' => $lampara_actual, 'fecha_lectura' => $fecha_inicial ]);
                    $dataProvider[$index] = $searchModel->searchLeds(Yii::$app->request->queryParams);

                    $series[$index] = [
                        [
                            'name' => $lamp->lampara,
                            'data' => $data,
                        ],
                    ];
                }

                $index ++;
            }
        }

        return $this->render('detalle-jaula', [
            'id_centro' => $id_centro,
            'series' => $series,
            'dataProvider' => $dataProvider,
            'model_centro' => $model_centro,
            'jaula_numero' => $id,
        ]);
    }

    /**
     * Detalle de una jaula
     * @return mixed
     */
    public function actionChartjaula( $id = 0, $id_centro = 0 )
    {
        return $this->renderPartial('_chart_jaula', []);
    }

    /**
     * Notificaciones del centro
     * @return mixed
     */
    public function actionNotifications( $id )
    {
        $model = $this->findModel($id);
        $model_centro_email = $model->centroEmailNotifications;
        $oldIDs = ArrayHelper::map($model_centro_email, 'id', 'id');

        if (Yii::$app->request->post()) {
            
            $model_centro_email = Model::createMultiple(CentroEmailNotification::classname(), $model_centro_email);
            Model::loadMultiple($model_centro_email, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($model_centro_email, 'id', 'id')));

            //Asociar centros
            if (isset($model_centro_email)) {
                if (! empty($deletedIDs)) {
                    CentroEmailNotification::deleteAll(['id' => $deletedIDs]);
                }
                foreach ($model_centro_email as $index => $email) {
                    if ($email->isNewRecord) {
                        $email->date = date('Y-m-d H:i:s');
                        $email->id_centro = $id;
                    }
                    $email->save(false);
                }
            }
        }

        return $this->render('notification', [
            'model' => $model,
            'model_centro_email' => (empty($model_centro_email)) ? [new CentroEmailNotification] : $model_centro_email
        ]);
    }

}
