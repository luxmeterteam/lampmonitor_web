<?php

namespace app\controllers;

use Yii;
use app\models\Empresa;
use app\models\EmpresaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Centro;
use app\models\Model;

/**
 * EmpresaController implements the CRUD actions for Empresa model.
 */
class EmpresaController extends Controller
{
    public $basePath ='/uploads';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Empresa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmpresaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Empresa models.
     * @return mixed
     */
    public function actionIndexempresa()
    {
        $searchModel = new EmpresaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-empresa', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Empresa model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Empresa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Empresa();
        $model_centros = [new Centro];

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            
            $model->fecha_creacion = date('Y-m-d H:i:s');
            $model->fecha_actualizacion = date('Y-m-d H:i:s');

            if ($model->save(false)) {

                $model->image = \yii\web\UploadedFile::getInstance($model, "image");

                //Crear carpeta Empresa
                $path = $this->basePath .'/empresas/'. $model->id . '/';
                if ( !is_dir(".".$path) ) {
                    mkdir( ".".$path, 0777, true );
                }

                if ( !empty($model->image->extension)) {
                    $file_name = "img_company_".$model->id.".{$model->image->extension}";

                    $path = $path . $file_name;
                    $model->url_imagen = $path;

                    if($model->save(false)){
                        $model->image->saveAs(".".$path);
                    }
                }

                $model_centros = Model::createMultiple(Centro::classname());
                Model::loadMultiple($model_centros, Yii::$app->request->post());

                //Asociar centros
                if (isset($model_centros)) {
                    foreach ($model_centros as $index => $centro) {
                        
                        $centro->fecha_creacion = date('Y-m-d H:i:s');
                        $centro->fecha_actualizacion = date('Y-m-d H:i:s');
                        $centro->id_empresa = $model->id;
                        if ($centro->save(false)) {
                            //Crear carpeta Centro
                            $path = $this->basePath .'/centros/'. $centro->id . '/';
                            if ( !is_dir(".".$path) ) {
                                mkdir( ".".$path, 0777, true );
                            }
                            $centro->image = \yii\web\UploadedFile::getInstance($centro, "[{$index}]image");

                            if ( !empty($centro->image->extension)) {
                                $file_name = "imagen_centro_".$model->id."_".$centro->id.".{$centro->image->extension}";

                                $path = $path . $file_name;
                                $centro->url_imagen = $path;
                                if($centro->save(false)){
                                    $centro->image->saveAs(".".$path);
                                }
                            }
                        }
                    }
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'model_centros' => (empty($model_centros)) ? [new Centro] : $model_centros,
        ]);
    }

    /**
     * Updates an existing Empresa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model_centros = $model->centros;
        $oldIDs = ArrayHelper::map($model_centros, 'id', 'id');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //Imagen de la empresa
            //Crear carpeta Empresa
            $path = $this->basePath .'/empresas/'. $model->id . '/';
            if ( !is_dir(".".$path) ) {
                mkdir( ".".$path, 0777, true );
            }
            $model->image = \yii\web\UploadedFile::getInstance($model, "image");

            if ( !empty($model->image->extension)) {
                
                if (!empty($model->url_imagen)) {
                    unlink('.'.$model->url_imagen);
                }
                $file_name = "img_company_".$model->id.".{$model->image->extension}";

                $path = $path . $file_name;
                $model->url_imagen = $path;
                if($model->save(false)){
                    $model->image->saveAs(".".$path);
                }
            }

            //$oldIDs = ArrayHelper::map($model_centros, 'id', 'id');
            $model_centros = Model::createMultiple(Centro::classname(), $model_centros);
            Model::loadMultiple($model_centros, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($model_centros, 'id', 'id')));

            //Asociar centros
            if (isset($model_centros)) {
                if (! empty($deletedIDs)) {
                    Centro::deleteAll(['id' => $deletedIDs]);
                }

                foreach ($model_centros as $index => $centro) {
                    if ($centro->isNewRecord) {
                        $centro->fecha_creacion = date('Y-m-d H:i:s');
                        $centro->id_empresa = $model->id;
                    }
                    $centro->fecha_actualizacion = date('Y-m-d H:i:s');
                    
                    if ($centro->save(false)) {
                        //Crear carpeta
                        $path = $this->basePath .'/centros/'. $centro->id . '/';
                        if ( !is_dir(".".$path) ) {
                            mkdir( ".".$path, 0777, true );
                        }
                        $centro->image = \yii\web\UploadedFile::getInstance($centro, "[{$index}]image");

                        if ( !empty($centro->image->extension)) {
                            
                            if (!empty($centro->url_imagen)) {
                                unlink('.'.$centro->url_imagen);
                            }
                            $file_name = "img_center".$model->id."_".$centro->id.".{$centro->image->extension}";

                            $path = $path . $file_name;
                            $centro->url_imagen = $path;
                            if($centro->save(false)){
                                $centro->image->saveAs(".".$path);
                            }
                        }
                    }
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'model_centros' => (empty($model_centros)) ? [new Centro] : $model_centros
        ]);
    }

    /**
     * Deletes an existing Empresa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Empresa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Empresa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Empresa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
