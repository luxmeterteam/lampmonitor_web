<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lm_bitacora_conexion".
 *
 * @property int $id
 * @property string $ultima_conexion
 * @property int $id_centro
 * 
 * @property Centro $centro
 */
class BitacoraConexion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lm_bitacora_conexion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ultima_conexion'], 'safe'],
            [['id_centro'], 'integer'],
            [['id_centro'], 'exist', 'skipOnError' => true, 'targetClass' => Centro::className(), 'targetAttribute' => ['id_centro' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ultima_conexion' => 'Ultima Conexion',
            'id_centro' => 'Id Centro',
        ];
    }

    /** 
    * @return \yii\db\ActiveQuery 
    */ 
    public function getCentro() 
    { 
        return $this->hasOne(Centro::className(), ['id' => 'id_centro']); 
    }
}
