<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empresa".
 *
 * @property int $id
 * @property string $nombre
 * @property string $codigo_empresa
 * @property string $fecha_creacion
 * @property string $fecha_actualizacion
 * @property string $url_imagen 
 * 
 * @property Centro[] $centros 
 */
class Empresa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresa';
    }

    public $image;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'codigo_empresa'], 'required'],
            [['fecha_creacion', 'fecha_actualizacion'], 'safe'],
            [['nombre', 'codigo_empresa'], 'string', 'max' => 45],
            [['url_imagen'], 'string', 'max' => 255],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, PNG, JPG, JPEG', 'maxFiles' => 1, 'maxSize' => 4194304],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => Yii::t('app', 'Company Name'),
            'codigo_empresa' => Yii::t('app', 'Company Code'),
            'fecha_creacion' => Yii::t('app', 'Created At'),
            'fecha_actualizacion' => Yii::t('app', 'Updated At'),
            'url_imagen' => 'Url Imagen',
            'image' => Yii::t('app', 'Company Logo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentros()
    {
        return $this->hasMany(Centro::className(), ['id_empresa' => 'id']);
    }
}
