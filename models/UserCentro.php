<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_centro".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_centro
 */
class UserCentro extends \yii\db\ActiveRecord
{
    public $id_empresa;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_centro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_centro', 'id_empresa'], 'integer'],
            [['id_empresa', 'id_centro'], 'required', 'when' =>  function ($model) {
                            return ($model->user->username != '');
                        }, 'whenClient' =>"function (username) {
                            return ($('#user-rol').val() != '' && $('#user-rol').val() == 'admin-centro');
                        }"
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_centro' => Yii::t('app', 'Center'),
            'id_empresa' => Yii::t('app', 'Company'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentro()
    {
        return $this->hasOne(Centro::className(), ['id' => 'id_centro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
