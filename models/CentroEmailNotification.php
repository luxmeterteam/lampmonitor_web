<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lm_centro_email_notification".
 *
 * @property int $id
 * @property string $cargo
 * @property string $email
 * @property int $id_user
 * @property int $id_centro
 * @property string $date
 *
 * @property Centro $centro
 * @property User $user
 * @property UserNotification[] $userNotifications
 */
class CentroEmailNotification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lm_centro_email_notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['id_user', 'id_centro'], 'integer'],
            [['date'], 'safe'],
            [['cargo'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['id_centro'], 'exist', 'skipOnError' => true, 'targetClass' => Centro::className(), 'targetAttribute' => ['id_centro' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cargo' => Yii::t('app', 'Position'),
            'email' => Yii::t('app', 'Email'),
            'id_user' => Yii::t('app', 'User'),
            'id_centro' => Yii::t('app', 'Center'),
            'date' => Yii::t('app', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentro()
    {
        return $this->hasOne(Centro::className(), ['id' => 'id_centro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserNotifications()
    {
        return $this->hasMany(UserNotification::className(), ['id_centro_email' => 'id']);
    }
}
