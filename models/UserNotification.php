<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lm_user_notification".
 *
 * @property int $id
 * @property string $date
 * @property int $type 0-Apagado, 1-Encendido, 2-Falla Lámpara
 * @property int $cant_lamps
 * @property string $lamps
 * @property int $id_user
 * @property int $id_centro
 *
 * @property Centro $centro
 * @property User $user
 */
class UserNotification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lm_user_notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['type', 'cant_lamps', 'id_user', 'id_centro'], 'integer'],
            [['lamps'], 'string', 'max' => 255],
            [['id_centro'], 'exist', 'skipOnError' => true, 'targetClass' => Centro::className(), 'targetAttribute' => ['id_centro' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'type' => 'Type',
            'cant_lamps' => 'Cant Lamps',
            'lamps' => 'Lamps',
            'id_user' => 'Id User',
            'id_centro' => 'Id Centro',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentro()
    {
        return $this->hasOne(Centro::className(), ['id' => 'id_centro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
