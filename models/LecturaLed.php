<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lm_lectura_led".
 *
 * @property int $id
 * @property int $codigo_centro
 * @property string $jaula
 * @property int $lampara
 * @property string $nserie
 * @property string $hora_lectura
 * @property string $fecha_lectura
 * @property string $fecha_actualizacion_remota
 * @property int $id_centro
 * @property int $id_estado
 * @property double $potencia 
 * @property double $voltaje 
 * @property double $corriente 
 * @property int $id_lampara 
 * @property int $id_local 
 * @property int $id_modulo
 * @property string $modulo
 *
 * @property Centro $centro
 * @property EstadoLed $estado
 */
class LecturaLed extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lm_lectura_led';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_centro', 'lampara', 'id_centro', 'id_estado', 'id_lampara', 'id_local', 'id_modulo'], 'integer'],
            [['hora_lectura', 'fecha_lectura', 'fecha_actualizacion_remota'], 'safe'],
            [['id_centro', 'id_estado'], 'required'],
            [['potencia', 'voltaje', 'corriente'], 'number'], 
            [['jaula'], 'string', 'max' => 20],
            [['nserie'], 'string', 'max' => 30],
            [['modulo'], 'string', 'max' => 150], 
            [['id_centro'], 'exist', 'skipOnError' => true, 'targetClass' => Centro::className(), 'targetAttribute' => ['id_centro' => 'id']],
            [['id_estado'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoLed::className(), 'targetAttribute' => ['id_estado' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_centro' => Yii::t('app', 'Center Code'),
            'jaula' => Yii::t('app', 'Cage'),
            'lampara' => Yii::t('app', 'Lamp'),
            'nserie' => Yii::t('app', 'Lamp Serial Number'),
            'hora_lectura' => Yii::t('app', 'Time'),
            'fecha_lectura' => Yii::t('app', 'Date'),
            'fecha_actualizacion_remota' => 'Fecha Actualización Remota',
            'id_centro' => Yii::t('app', 'Center'),
            'id_estado' => Yii::t('app', 'Status'),
            'potencia' => Yii::t('app', 'Potency'),
            'voltaje' => Yii::t('app', 'Voltage'),
		    'corriente' => Yii::t('app', 'Current'),
            'id_lampara' => 'Id Lampara',
            'id_local' => 'Id Local',
            'id_modulo' => 'Id Modulo',
		    'modulo' => 'Modulo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentro()
    {
        return $this->hasOne(Centro::className(), ['id' => 'id_centro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstado()
    {
        return $this->hasOne(EstadoLed::className(), ['id' => 'id_estado']);
    }
}
