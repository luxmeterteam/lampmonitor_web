<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lm_estado_led".
 *
 * @property int $id
 * @property string $nombre
 * @property string $color
 * 
 * @property LecturaLed[] $lmLecturaLeds 
 */
class EstadoLed extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lm_estado_led';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 45],
            [['color'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'color' => 'Color',
        ];
    }

    /** 
    * @return \yii\db\ActiveQuery 
    */ 
    public function getLecturaLeds() 
    { 
        return $this->hasMany(LecturaLed::className(), ['id_estado' => 'id']); 
    } 
}
