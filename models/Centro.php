<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "centro".
 *
 * @property int $id
 * @property string $nombre
 * @property int $codigo_centro
 * @property string $direccion
 * @property string $fecha_creacion
 * @property string $fecha_actualizacion
 * @property int $id_empresa
 * @property string $url_imagen
 *
 * @property Empresa $empresa
 * @property LecturaLed[] $lecturaLeds
 */
class Centro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'centro';
    }

    public $image;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_centro', 'nombre', 'id_empresa'], 'required'],
            [['codigo_centro', 'id_empresa'], 'integer'],
            [['fecha_creacion', 'fecha_actualizacion'], 'safe'],
            [['image'], 'required', 'on'=> 'create'],
            [['nombre'], 'string', 'max' => 45],
            [['direccion'], 'string', 'max' => 150],
            [['url_imagen'], 'string', 'max' => 255],
            [['id_empresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['id_empresa' => 'id']],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, PNG, JPG, JPEG', 'maxFiles' => 1, 'maxSize' => 4194304],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => Yii::t('app', 'Farm Name'),
            'codigo_centro' => Yii::t('app', 'Farm Code'),
            'direccion' => Yii::t('app', 'Address'),
            'fecha_creacion' => Yii::t('app', 'Created At'),
            'fecha_actualizacion' => Yii::t('app', 'Created At'),
            'id_empresa' => Yii::t('app', 'Company'),
            'url_imagen' => Yii::t('app', 'Farm Image'),
            'image' => Yii::t('app', 'Farm Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'id_empresa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLecturaLeds()
    {
        return $this->hasMany(LecturaLed::className(), ['id_centro' => 'id']);
    }

    /**
	 * @return \yii\db\ActiveQuery
     */
    public function getCentroEmailNotifications() 
    { 
        return $this->hasMany(CentroEmailNotification::className(), ['id_centro' => 'id']); 
    }
          
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getUserNotifications() 
    { 
        return $this->hasMany(UserNotification::className(), ['id_centro' => 'id']); 
    }

    /** 
    * @return \yii\db\ActiveQuery 
    */ 
    public function getBitacoraConexions() 
    { 
        return $this->$this->hasOne(BitacoraConexion::className(), ['id_centro' => 'id']); 
    }
}
