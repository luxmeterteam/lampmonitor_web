<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lm_event_centro".
 *
 * @property int $id
 * @property int $cant_lamps_on
 * @property int $cant_lamps_off
 * @property int $cant_lamps_reconnected
 * @property int $cant_lamps_not
 * @property int $cant_lamps
 * @property int $mail_send
 * @property string $date
 * @property int $index
 * @property int $id_centro
 *
 * @property Centro $centro
 */
class EventCentro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lm_event_centro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cant_lamps_on', 'cant_lamps_off', 'cant_lamps_reconnected', 'cant_lamps_not', 'cant_lamps', 'mail_send', 'index', 'id_centro'], 'integer'],
            [['date'], 'safe'],
            [['id_centro'], 'required'],
            [['id_centro'], 'exist', 'skipOnError' => true, 'targetClass' => Centro::className(), 'targetAttribute' => ['id_centro' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cant_lamps_on' => 'Cant Lamps On',
            'cant_lamps_off' => 'Cant Lamps Off',
            'cant_lamps_reconnected' => 'Cant Lamps Reconnected',
            'cant_lamps_not' => 'Cant Lamps Not',
            'cant_lamps' => 'Cant Lamps',
            'mail_send' => 'Mail Send',
            'date' => 'Date',
            'index' => 'Index',
            'id_centro' => 'Id Centro',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentro()
    {
        return $this->hasOne(Centro::className(), ['id' => 'id_centro']);
    }
}
