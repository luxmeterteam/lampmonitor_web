<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LecturaLed;

/**
 * LecturaLedSearch represents the model behind the search form of `app\models\LecturaLed`.
 */
class LecturaLedSearch extends LecturaLed
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'codigo_centro', 'lampara', 'id_centro', 'id_estado'], 'integer'],
            [['jaula', 'nserie', 'hora_lectura', 'fecha_lectura', 'fecha_actualizacion_remota', 'modulo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LecturaLed::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'codigo_centro' => $this->codigo_centro,
            'lampara' => $this->lampara,
            'hora_lectura' => $this->hora_lectura,
            'fecha_lectura' => $this->fecha_lectura,
            'fecha_actualizacion_remota' => $this->fecha_actualizacion_remota,
            'id_centro' => $this->id_centro,
            'id_estado' => $this->id_estado,
        ]);

        $query->andFilterWhere(['like', 'jaula', $this->jaula])
            ->andFilterWhere(['like', 'nserie', $this->nserie])
            ->andFilterWhere(['like', 'modulo', $this->modulo]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchLeds($params)
    {
        $this->load($params);

        $query = LecturaLed::find()
            ->where(['id_centro' => $this->id_centro, 'jaula' => $this->jaula, 'lampara' => $this->lampara])
            ->andWhere(['>=', 'fecha_lectura', $this->fecha_lectura])
            ->orderBy('lampara, fecha_lectura, hora_lectura ASC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchCentro($params)
    {
        $this->load($params);

        $query = LecturaLed::find()
            ->where(['id_centro' => $this->id_centro])
            ->andWhere(['like', 'jaula', (string)$this->jaula.'%', false ])
            ->andWhere(['>=', 'fecha_lectura', $this->fecha_lectura])
            ->andWhere(['like', 'modulo', (string)$this->modulo.'%', false ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
