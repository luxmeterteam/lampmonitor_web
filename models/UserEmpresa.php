<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_empresa".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_empresa
 */
class UserEmpresa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_empresa'], 'integer'],
            [['id_empresa'], 'required', 'when' =>  function ($model) {
                            return ($model->user->username != '');
                        }, 'whenClient' =>"function (username) {
                            return ($('#user-rol').val() != '' && $('#user-rol').val() == 'admin-empresa');
                        }"
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_empresa' => Yii::t('app', 'Company'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'id_empresa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
