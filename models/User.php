<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $nombre
 * @property string $apellido
 * @property string $url_imagen
 * @property string $direccion
 * @property string $telefono
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 10;
    public $rol;
    public $pass_aux;
    public $newPassword;
    public $retypePassword;
    public $prefix;
    public $image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'rol'], 'required'],
            [['status'], 'integer'],
            [['username', 'auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token', 'email', 'url_imagen', 'direccion'], 'string', 'max' => 255],
            [['created_at', 'updated_at'], 'safe'],
            [['created_at', 'updated_at'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['nombre', 'apellido'], 'string', 'max' => 25],
            [['telefono'], 'string', 'max' => 40],
            //['email', 'unique', 'targetAttribute' => ['email'], 'message' => Yii::t('app', 'Email already exists')],
            'usernameUnique'   => [
                'username',
                'unique',
                'message' => Yii::t('app', 'Username already exists'),
            ],
            'usernameTrim'     => ['username', 'trim'],
            [['email'], 'email'],
            [['newPassword'], 'string', 'min' => 6],
            [['retypePassword'], 'compare', 'compareAttribute' => 'newPassword', 'message' => Yii::t('app', 'Passwords do not match'),],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, PNG, JPG, JPEG', 'maxFiles' => 1, 'maxSize' => 4194304],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => Yii::t('app', 'Username'),
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'nombre' => Yii::t('app', 'Name'),
            'apellido' => Yii::t('app', 'Last Name'),
            'rol' => Yii::t('app', 'Role'),
            'newPassword' => Yii::t('app', 'New Password'),
            'retypePassword' => Yii::t('app', 'Retype Password'),
            'url_imagen' => Yii::t('app', 'Retype Password'),
            'direccion' => Yii::t('app', 'Address'),
            'telefono' => Yii::t('app', 'Phone Number'),
            'image' => Yii::t('app', 'Profile Image'),
        ];
    }

    /**
     * @inheritdoc
    */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($pass)
    {
        $length = 10;
        $chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
        shuffle($chars);
        $password = implode(array_slice($chars, 0, $length));
        //$this->pass_aux = $password;
        $this->pass_aux = $pass;
        $this->password_hash = Yii::$app->security->generatePasswordHash($pass);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
	 */
    public function getUserNotifications()
    {
        return $this->hasMany(UserNotification::className(), ['id_user' => 'id']);
    }
}